#ifndef dynamic_library_handler_hpp
#define dynamic_library_handler_hpp

#include <functional>
#include <string>
#include <sstream>
#include "logger.hpp"

#if defined(__unix__) || defined(__APPLE__)
#include <dlfcn.h>
#else
// TODO: include headers for loading dynamic libraries on Windows
#endif

class DynamicLibraryHandler
{
public:
	DynamicLibraryHandler(const std::string inDynamicLibraryPath);
	~DynamicLibraryHandler();
	
	bool isDynamicLibraryValid() const;
	
	template<typename T>
	std::function<T> loadFunctionFromDynamicLibrary(const std::string inFunctionName) const
	{
		if (dynamicLibraryHandle == nullptr)
		{
			return std::function<T>();
		}
		
#if defined(__unix__) || defined(__APPLE__)
		void* functionHandle = dlsym(dynamicLibraryHandle, inFunctionName.c_str());
		const char* errorMessage = dlerror();
		if (errorMessage != nullptr)
		{
			std::stringstream functionLoadErrorMessageStream;
			functionLoadErrorMessageStream << "Couldn't find function in dynamic library. Error message: ";
			functionLoadErrorMessageStream << errorMessage;
			Logger::logErrorMessage(functionLoadErrorMessageStream.str());
			return std::function<T>();
		}
		return std::function<T>(reinterpret_cast<T*>(functionHandle));
#else
		// TODO: implement getting function from dynamic library on Windows
		return std::function<T>();
#endif
	}
	
private:
	void* dynamicLibraryHandle = nullptr;
};

#endif /* dynamic_library_handler_hpp */
