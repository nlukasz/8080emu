#include <iomanip>
#include <sstream>
#include "printing_functions.hpp"

std::string getNumberInHexString(const unsigned int inNumberToPrint, const unsigned int inWidth)
{
	std::stringstream hexNumberStirngStream;
	hexNumberStirngStream << std::hex << std::uppercase << std::setfill('0') << std::setw(inWidth);
	hexNumberStirngStream << inNumberToPrint;
	hexNumberStirngStream << std::dec << std::nouppercase << std::setfill('\0') << std::setw(0);
	return hexNumberStirngStream.str();
}

std::string getInstructionString(const CPUInstruction& inInstruction, const bool inPrintEndLine)
{
	std::stringstream instructionStirngStream;
	instructionStirngStream << inInstruction.getReadableName();
	if (inInstruction.getArgumentsBytesCount() > 0)
	{
		instructionStirngStream << " ";
		for (unsigned int i = (inInstruction.getArgumentsBytesCount()); i > 0; --i)
		{
			uint8_t argumentValue = 0;
			if (!inInstruction.getArgumentValue(i - 1, argumentValue))
			{
				return "";
			}
			instructionStirngStream << getNumberInHexString(argumentValue);
		}
	}
	if (inPrintEndLine)
	{
		instructionStirngStream << std::endl;
	}
	return instructionStirngStream.str();
}
