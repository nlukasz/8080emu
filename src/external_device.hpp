#ifndef external_device_hpp
#define external_device_hpp

#include <chrono>
#include <unordered_map>
#include <unordered_set>
#include <thread>
#include "memory.hpp"

struct ExternalDeviceSettings
{
	ExternalDeviceSettings(
						   const bool inUsesMainThreadLoop = false,
						   const bool inUsesExternalThreadLoop = false,
						   const bool inAllReadPortsAvailable = false,
						   const std::unordered_set<uint8_t> inAvailableReadPorts = {},
						   const bool inAllWritePortsAvailable = false,
						   const std::unordered_set<uint8_t> inAvailableWritePorts = {},
						   const bool inGeneratesInterrupts = false
						   ) :
	usesMainThreadLoop(inUsesMainThreadLoop),
	usesExternalThreadLoop(inUsesExternalThreadLoop),
	allReadPortsAvailable(inAllReadPortsAvailable),
	availableReadPorts(inAvailableReadPorts),
	allWritePortsAvailable(inAllWritePortsAvailable),
	availableWritePorts(inAvailableWritePorts),
	generatesInterrupts(inGeneratesInterrupts) {}
	
	const bool usesMainThreadLoop;
	const bool usesExternalThreadLoop;
	const bool allReadPortsAvailable;
	const std::unordered_set<uint8_t> availableReadPorts;
	const bool allWritePortsAvailable;
	const std::unordered_set<uint8_t> availableWritePorts;
	const bool generatesInterrupts;
};

class ExternalDevice
{
public:
	~ExternalDevice();
	void initializeExternalDevice();
	void startExternalDevice();
	void stopExternalDevice();
	bool readByteAtPort(const uint8_t inPort, uint8_t& outByte) const;
	bool writeByteAtPort(const uint8_t inByte, const uint8_t inPort);
	bool isReadPortAvailable(const uint8_t inPort) const;
	bool isWritePortAvailable(const uint8_t inPort) const;
	bool canGenerateInterrupts() const;
	bool consumeInterrupt(uint8_t& outInstructionByte);
	uint32_t performMainThreadUpdate();
	const ExternalDeviceSettings getExternalDeviceSettings() const;
	
protected:
	ExternalDevice(const ExternalDeviceSettings inExternalDeviceSettings, const Memory& inMemory) : externalDeviceSettings(inExternalDeviceSettings), memory(inMemory) {}
	virtual void doExternalDeviceInitialization() {};
	virtual uint32_t externalThreadDeviceUpdate(const float inTimeSinceLastUpdate) { return 0; }
	virtual uint32_t mainThreadDeviceUpdate(const float inTimeSinceLastUpdate) { return 0; }
	virtual void handleByteWriteAtPort(const uint8_t inPort) {};
	bool getWriteByteAtPort(const uint8_t inPort, uint8_t& outByte) const;
	
	const Memory& memory;
	std::unordered_map<uint8_t, uint8_t> readBytes;
	uint8_t interruptInstructionByte = 0;
	bool generateInterrupt = false;
	
private:
	void startExternalThreadLoop();
	void stopExternalThreadLoop();
	
	const ExternalDeviceSettings externalDeviceSettings;
	std::unordered_map<uint8_t, uint8_t> writeBytes;
	bool deviceInitialized = false;
	
	// Main thread
	std::chrono::time_point<std::chrono::steady_clock> lastMainThreadUpdateTime;
	bool firstMainThreadUpdateFinished = false;
	
	// External thread
	std::chrono::time_point<std::chrono::steady_clock> lastExternalThreadUpdateTime;
	bool externalThreadLoopRunning = false;
	std::thread* deviceLoopExternalThread = nullptr;
};

#endif /* external_device_hpp */
