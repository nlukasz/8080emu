#include "processor.hpp"
#include "byte_functions.hpp"
#ifdef DEBUG_MESSAGES_ENABLED
#include <sstream>
#include "logger.hpp"
#include "printing_functions.hpp"
#endif

// Getters
// Registers getters
uint16_t Processor::getProgramCounter() const
{
	return PC;
}

uint16_t Processor::getBCPairValue() const
{
	return combineTwoBytesToWord(B, C);
}

uint16_t Processor::getDEPairValue() const
{
	return combineTwoBytesToWord(D, E);
}

uint16_t Processor::getHLPairValue() const
{
	return combineTwoBytesToWord(H, L);
}

// Memory getters
const Memory& Processor::getMemory() const
{
	return memory;
}

bool Processor::getByteAtProgramCounter(uint8_t& outByte) const
{
	return memory.getByteAtAddress(PC, outByte);
}

bool Processor::getByteAtAddressStoredInHL(uint8_t& outByteValue) const
{
	return memory.getByteAtAddress(getHLPairValue(), outByteValue);
}

bool Processor::getBytesPairStoredAtSP(uint8_t& outHighByte, uint8_t& outLowByte) const
{
	return memory.getBytePairAtAddress(SP, outHighByte, outLowByte);
}

bool Processor::getByteInMemory(const uint16_t inIndex, uint8_t& outByte) const
{
	// TODO: remove and replace with getMemory().getByteAtAddress
	return memory.getByteAtAddress(inIndex, outByte);
}

bool Processor::getByteInMemory(const uint8_t inAddressHighByte, const uint8_t inAddressLowByte, uint8_t& outByte) const
{
	uint16_t address = combineTwoBytesToWord(inAddressHighByte, inAddressLowByte);
	return getByteInMemory(address, outByte);
}

// Other getters
bool Processor::areInterruptsEnabled() const
{
	return interruptsEnabled;
}

bool Processor::isHalted() const
{
	return halted;
}

// Setters
// Registers setters
void Processor::incraseProgramCounterBy()
{
	++PC;
}

void Processor::setHLPairValue(const uint16_t inNewPairValue)
{
	splitWordToTwoBytes(H, L, inNewPairValue);
}

void Processor::setPC(const uint8_t inHighByte, const uint8_t inLowByte)
{
	PC = combineTwoBytesToWord(inHighByte, inLowByte);
}

// Flags update
void Processor::updateFlagsForByte(const uint8_t inByte)
{
	Flags.Z = (inByte == 0);
	Flags.P = isNumberOfSetBitsInByteEven(inByte);
	Flags.S = ((inByte & 0x80) != 0);
}

void Processor::updateFlagsForAccumulator()
{
	updateFlagsForByte(A);
}

// Memory setters
bool Processor::setByteInMemory(const uint16_t inMemoryIndex, const uint8_t inByte)
{
	return memory.setByteAtAddress(inMemoryIndex, inByte);
}

bool Processor::setByteInMemory(const uint8_t inAddressHighByte, const uint8_t inAddressLowByte, const uint8_t inByte)
{
	unsigned int address = combineTwoBytesToWord(inAddressHighByte, inAddressLowByte);
	return setByteInMemory(address, inByte);
}

bool Processor::setByteAtAddressStoredInHL(const uint8_t inByte)
{
	return setByteInMemory(getHLPairValue(), inByte);
}

// Stack operations
bool Processor::pushBytesPairOnStack(const uint8_t inHighByte, const uint8_t inLowByte)
{
	if (!memory.setBytesPairAtAddress(SP - 1, inHighByte, inLowByte))
	{
		return false;
	}
	SP -= 2;
	return true;
}

bool Processor::pushOnStack(const uint16_t inValue)
{
	return pushBytesPairOnStack(
		static_cast<uint8_t>((inValue >> 8) & 0xFF),
		static_cast<uint8_t>(inValue & 0xFF));
}

bool Processor::popFromStackToBytes(uint8_t& outHighByte, uint8_t& outLowByte)
{
	if (!memory.getBytePairAtAddress(SP, outHighByte, outLowByte))
	{
		return false;
	}
	SP += 2;
	return true;
}

bool Processor::popFromStack(uint16_t& outValue)
{
	uint8_t highByte = 0;
	uint8_t lowByte = 0;
	if (!popFromStackToBytes(highByte, lowByte))
	{
		return false;
	}
	outValue = static_cast<uint16_t>(lowByte);
	outValue = outValue | (static_cast<uint16_t>(highByte) << 8);
	return true;
}

// Other setters
void Processor::setInterruptsEnabled(const bool inEnabled)
{
	interruptsEnabled = inEnabled;
}

void Processor::setHalted(const bool inHalted)
{
	halted = inHalted;
}

// External devices connections
bool Processor::addInputDeviceConnection(const uint8_t inPort, InputDeviceConnection inConnection)
{
	if (hasInputDeviceConnectedToPort(inPort))
	{
		return false;
	}
	connectedInputDevices.emplace(inPort, inConnection);
	return true;
}

bool Processor::addOutputDeviceConnection(const uint8_t inPort, OutputDeviceConnection inConnection)
{
	if (hasOutputDeviceConnectedToPort(inPort))
	{
		return false;
	}
	connectedOutputDevices.emplace(inPort, inConnection);
	return true;
}

bool Processor::hasInputDeviceConnectedToPort(const uint8_t inPort) const
{
	auto inputDeviceIterator = connectedInputDevices.find(inPort);
	return (inputDeviceIterator != connectedInputDevices.end());
}

bool Processor::hasOutputDeviceConnectedToPort(const uint8_t inPort) const
{
	auto outputDeviceIterator = connectedOutputDevices.find(inPort);
	return (outputDeviceIterator != connectedOutputDevices.end());
}

const InputDeviceConnection& Processor::getInputDeviceConnectionAtPort(const uint8_t inPort) const
{
	return connectedInputDevices.find(inPort)->second;
}

const OutputDeviceConnection& Processor::getOutputDeviceConnectionAtPort(const uint8_t inPort) const
{
	return connectedOutputDevices.find(inPort)->second;
}

// Debug functions
#ifdef DEBUG_MESSAGES_ENABLED
void Processor::printCPUState() const
{
	std::stringstream cpuStateStream;
	// Registers
	cpuStateStream << "A = ";
	cpuStateStream << getNumberInHexString(A);
	cpuStateStream << " | B = ";
	cpuStateStream << getNumberInHexString(B);
	cpuStateStream << " | C = ";
	cpuStateStream << getNumberInHexString(C);
	cpuStateStream << " | D = ";
	cpuStateStream << getNumberInHexString(D);
	cpuStateStream << " | E = ";
	cpuStateStream << getNumberInHexString(E);
	cpuStateStream << " | H = ";
	cpuStateStream << getNumberInHexString(H);
	cpuStateStream << " | L = ";
	cpuStateStream << getNumberInHexString(L);
	cpuStateStream << " | PC = ";
	cpuStateStream << getNumberInHexString(PC, 4);
	cpuStateStream << " | SP = ";
	cpuStateStream << getNumberInHexString(SP, 4);
	cpuStateStream << std::endl;
	
	// Flags
	cpuStateStream << "CY = " << Flags.C << " | P = " << Flags.P << " | Z = " << Flags.Z << " | AC = " << Flags.AC << " | S = " << Flags.S << " || Interrupts = " << interruptsEnabled;
	Logger::logMessage(cpuStateStream.str());
}
#endif
