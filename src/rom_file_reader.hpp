#ifndef rom_file_reader_hpp
#define rom_file_reader_hpp

#include <string>
#include <fstream>
#include <utility>

class RomFileReader
{
public:
	RomFileReader(std::string inROMFilePath, unsigned int inBufferSize) : mROMFilePath(inROMFilePath), mBufferSize(inBufferSize) {}
	~RomFileReader();
	
	bool openROMFile();
	bool getNextByte(uint8_t& outByte);
	bool isEOF() const;
	unsigned int getCurrentlyOpenedROMFileSize() const;
	
private:
	std::string mROMFilePath = "";
	unsigned int mBufferSize = 0;
	unsigned int mFileSize = 0;
	std::ifstream mROMFileStream;
	char* mReadBuffer = nullptr;
	std::pair<unsigned int, unsigned int> mBufferRange = std::make_pair(0, 0);
	unsigned int mCurrentPositionInFile = 0;
	
	bool readBytesToBuffer(unsigned int inPosition);
	bool getByteAt(unsigned int inPosition, uint8_t& outByte);
};

#endif /* rom_file_reader_hpp */
