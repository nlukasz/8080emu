#include "byte_functions.hpp"

uint16_t combineTwoBytesToWord(const uint8_t inHigherByte, const uint8_t inLowerByte)
{
	return (static_cast<uint16_t>(inHigherByte) << 8) | static_cast<uint16_t>(inLowerByte);
}

void splitWordToTwoBytes(uint8_t& inHigherByte, uint8_t& inLowerByte, const uint16_t inWord)
{
	inHigherByte = static_cast<uint8_t>(inWord >> 8);
	inLowerByte = static_cast<uint8_t>(inWord & 0xFF);
}

bool isNumberOfSetBitsInByteEven(const uint8_t inByte)
{
	unsigned int numberOfSetBits = 0;
	for (unsigned int i = 0; i < 8; ++i)
	{
		if (((inByte >> i) & 0x01) > 0)
		{
			++numberOfSetBits;
		}
	}
	return (numberOfSetBits & 0x01) == 0;
}

void setBitInByte(const bool inNewBitState, const uint8_t inBitMask, uint8_t& outByte)
{
	if (inNewBitState)
	{
		outByte |= inBitMask;
	}
	else
	{
		outByte &= ~inBitMask;
	}
}
