#ifndef logger_hpp
#define logger_hpp

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

class Logger
{
public:
	static void logMessage(const std::string inMessage, const bool inAddNewLine = true);
	static void logErrorMessage(const std::string inMessage, const bool inAddNewLine = true);
	static void logNewLine();
	
	~Logger();
	
private:
	Logger();
	inline bool canLogToFile() const;
	void logMessageInternal(const std::string& inMessage, const bool& inAddNewLine);
	
	static Logger staticLogger;
	static const int MAXIMUM_LOG_FILE_LINES_COUNT = 10000;
#ifndef DO_NOT_LOG_TO_STD_OUT
	static const bool printToStandardOutput = true;
#else
	static const bool printToStandardOutput = false;
#endif
	static const bool printToFile = false;
	static const std::string logFilePath;
	std::ofstream loggerFileStream;
	std::vector<std::string> fileLines;
};

#endif /* logger_hpp */
