#ifndef space_invaders_shift_device_hpp
#define space_invaders_shift_device_hpp

#include "external_device.hpp"

enum SpaceInvadersShiftDeviceWritePorts
{
	ByteSendPort = 0,
	OffsetPort
};

enum SpaceInvadersShiftDeviceReadPorts
{
	ResultPort = 0
};

class SpaceInvadersShiftDevice : public ExternalDevice
{
public:
	SpaceInvadersShiftDevice() : ExternalDevice(getShiftDeviceSettings(), Memory(0)) {}
	static ExternalDeviceSettings getShiftDeviceSettings();
	
protected:
	void doExternalDeviceInitialization() override;
	void handleByteWriteAtPort(const uint8_t inPort) override;
	
private:
	uint16_t storedWord = 0;
	uint8_t offset = 0;
	
	inline void updateResult();
	void updateStoredWord();
	void updateOutputByte();
};

#endif /* space_invaders_shift_device_hpp */
