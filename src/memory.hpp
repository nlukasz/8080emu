#ifndef memory_hpp
#define memory_hpp

#include <cinttypes>

class Memory
{
public:
	Memory(const unsigned int inMemorySize);
	~Memory();
	
	// Getters
	unsigned int getMemorySize() const;
	bool getByteAtAddress(const uint16_t inAddress, uint8_t& outByte) const;
	bool getBytePairAtAddress(const uint16_t inAddress, uint8_t& outHighByte, uint8_t& outLowByte) const;
	
	// setters
	bool setByteAtAddress(const uint16_t inAddress, const uint8_t inByte);
	bool setBytesPairAtAddress(const uint16_t inAddress, const uint8_t inHighByte, const uint8_t inLowByte);
	
	static const unsigned int MAX_MEMORY_ADDRESS = 65535;
	
private:
	// Helper functions
	bool validateMemoryAddress(const uint16_t inMemoryIndex) const;
	inline bool isValid() const;
	
	const unsigned int memorySize;
	uint8_t *memory = nullptr;
};

#endif /* memory_hpp */
