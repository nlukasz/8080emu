#ifndef space_invaders_video_device_hpp
#define space_invaders_video_device_hpp

#include "external_device.hpp"
#include <SDL2/SDL.h>
#include <SDL2_mixer/SDL_mixer.h>

enum SpaceInvadersInputPorts
{
	INP0 = 0,
	INP1 = 1,
	INP2 = 2
};

enum SpaceInvadersAudioPorts
{
	SOUND1 = 0,
	SOUND2 = 1
};

class SpaceInvadersVideoDevice : public ExternalDevice
{
public:
	SpaceInvadersVideoDevice(const Memory& inMemory) : ExternalDevice(getVideoDeviceSettings(), inMemory) {}
	~SpaceInvadersVideoDevice();
	static ExternalDeviceSettings getVideoDeviceSettings();
	
protected:
	void doExternalDeviceInitialization() override;
	uint32_t mainThreadDeviceUpdate(const float inTimeSinceLastUpdate) override;
	void handleByteWriteAtPort(const uint8_t inPort) override;
	
private:
	struct SoundData
	{
		Mix_Chunk* sound = nullptr;
		bool repeated = false;
		int currentChannel = -1;
	};
	
	void initSDL();
	void initSounds();
	void closeSDL();
	void updateEvents();
	inline void setPixelColour(const unsigned int& inX, const unsigned int& inY) const;
	void drawScreen(const unsigned int inStartLine = 0, const unsigned int inEndLine = SCREEN_HEIGHT);
	void playSound(const uint8_t inSoundByte, std::unordered_map<uint8_t, SoundData>& soundsMap);
	
	static const unsigned int SCREEN_WIDTH = 256;
	static const unsigned int SCREEN_HEIGHT = 224;
	static const uint16_t SCREEN_STARTING_ADDRESS = 0x2400;
	static const float INTERRUPT_TIME;
	bool endOfTheFrame = false;
	float timeSinceLastInterrupt = 0.0f;
	bool SDLInitialized = false;
	bool SDLMixerInitialized = false;
	SDL_Window* window = nullptr;
	SDL_Renderer* renderer = nullptr;
	bool quitRequested = false;
	std::unordered_map<uint8_t, SoundData> sounds1Map;
	std::unordered_map<uint8_t, SoundData> sounds2Map;
};

#endif /* space_invaders_video_device_hpp */
