#ifndef emulator_hpp
#define emulator_hpp

#include <string>
#include <unordered_map>
#include "instruction_data.hpp"
#include "processor.hpp"
#include "rom_file_reader.hpp"

enum class EEmulationResult
{
	Success = 0,
	CantOpenROMFile,
	FileReadError,
	UnrecognizedCPUInstruction,
	WrongCPUInstructionArgument,
	UnimplementedInstruction,
	InstructionExecutionError,
	ProgramDataReadError,
	ProgramBiggerThanAvailableMemory,
	ExternalDevicesSetupFailed,
	ExternalDeviceMainThreadUpdateError
};

std::pair<EEmulationResult, std::string> emulateROM(std::string inROMFilePath, unsigned int inReadBufferSize, const uint16_t inProgramStartAddress = 0);
std::string getEmulationErrorMessage(const std::pair<EEmulationResult, std::string>& emulationResult);

#endif /* emulator_hpp */
