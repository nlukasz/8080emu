#ifndef device_connector_hpp
#define device_connector_hpp

#include "external_device.hpp"

class DeviceConnection
{
public:
	DeviceConnection(ExternalDevice& inExternalDevice, const uint8_t inExternalDevicePort) : externalDevice(inExternalDevice), externalDevicePort(inExternalDevicePort) {}
	DeviceConnection(const DeviceConnection& inOtherConnection) : externalDevice(inOtherConnection.externalDevice), externalDevicePort(inOtherConnection.externalDevicePort) {};
	
protected:
	inline uint8_t getExternalDevicePort() const { return externalDevicePort; }
	
	ExternalDevice& externalDevice;
	
private:
	const uint8_t externalDevicePort;
};

class InputDeviceConnection : public DeviceConnection
{
public:
	InputDeviceConnection(ExternalDevice& inExternalDevice, const uint8_t inExternalDevicePort) : DeviceConnection(inExternalDevice, inExternalDevicePort) {}
	InputDeviceConnection(const InputDeviceConnection& inOtherDevice) : DeviceConnection(inOtherDevice) {}
		
	bool getInputByte(uint8_t& outByte) const;
};

class OutputDeviceConnection : public DeviceConnection
{
public:
	OutputDeviceConnection(ExternalDevice& inExternalDevice, const uint8_t inExternalDevicePort) : DeviceConnection(inExternalDevice, inExternalDevicePort) {}
	OutputDeviceConnection(const OutputDeviceConnection& inOtherDevice) : DeviceConnection(inOtherDevice) {}
	
	bool passOutputByte(const uint8_t inByte) const;
};

#endif /* device_connector_hpp */
