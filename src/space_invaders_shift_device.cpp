#include "space_invaders_shift_device.hpp"

ExternalDeviceSettings SpaceInvadersShiftDevice::getShiftDeviceSettings()
{
	return ExternalDeviceSettings(false, false, false, { SpaceInvadersShiftDeviceReadPorts::ResultPort }, false, { SpaceInvadersShiftDeviceWritePorts::ByteSendPort, SpaceInvadersShiftDeviceWritePorts::OffsetPort }, false);
}

void SpaceInvadersShiftDevice::doExternalDeviceInitialization()
{
	readBytes[SpaceInvadersShiftDeviceReadPorts::ResultPort] = 0;
}

void SpaceInvadersShiftDevice::handleByteWriteAtPort(const uint8_t inPort)
{
	if (inPort == SpaceInvadersShiftDeviceWritePorts::ByteSendPort)
	{
		updateStoredWord();
	}
	else if (inPort == SpaceInvadersShiftDeviceWritePorts::OffsetPort)
	{
		updateOutputByte();
	}
}

void SpaceInvadersShiftDevice::updateResult()
{
	readBytes[SpaceInvadersShiftDeviceReadPorts::ResultPort] = static_cast<uint8_t>(storedWord >> (8  - offset));
}

void SpaceInvadersShiftDevice::updateStoredWord()
{
	uint8_t newByte = 0;
	getWriteByteAtPort(SpaceInvadersShiftDeviceWritePorts::ByteSendPort, newByte);
	storedWord = (static_cast<uint16_t>(newByte) << 8) | (storedWord >> 8);
	updateResult();
}

void SpaceInvadersShiftDevice::updateOutputByte()
{
	getWriteByteAtPort(SpaceInvadersShiftDeviceWritePorts::OffsetPort, offset);
	offset = offset & 0x7;
	updateResult();
}
