#include <sstream>
#include "app_info.hpp"
#include "program_defaults.hpp"
#include "arguments_handler.hpp"
#include "disassembler.hpp"
#include "emulator.hpp"
#include "logger.hpp"

int main(int argc, char** argv)
{
	int readBufferSize = DEFAULT_BUFFER_SIZE;
	bool runInDisassemblyMode = false;
	uint16_t programStartAddress = 0;
	
	std::vector<ProgramArgumentData> argumentsData;
	std::string ROMPath;
	std::pair<EArgumentParseResult, std::string> argumentsParseResult = parseProgramArguments(argc, const_cast<const char* const *>(argv), argumentsData, ROMPath);
	if (argumentsParseResult.first != EArgumentParseResult::Success)
	{
		Logger::logErrorMessage(getArgumentParseErrorMessage(argumentsParseResult));
		return static_cast<int>(argumentsParseResult.first);
	}
	
	for (ProgramArgumentData argumentData : argumentsData)
	{
		switch(argumentData.argumentType)
		{
			case EArgumentOptionType::DisassemblyROM:
				runInDisassemblyMode = true;
				break;
			case EArgumentOptionType::SetBufferSize:
				readBufferSize = argumentData.integerValue;
				break;
			case EArgumentOptionType::ROMStartAddress:
				programStartAddress = static_cast<uint16_t>(argumentData.integerValue);
				break;
			default:
				break;
		}
	}
	
#ifdef DEBUG_MESSAGES_ENABLED
	{
		std::stringstream disassemblyLogStream;
		disassemblyLogStream << "Disassembly = " << runInDisassemblyMode << ", Buffer = " << readBufferSize << ", ROM = " << ROMPath;
		Logger::logMessage(disassemblyLogStream.str());
	}
#endif
	
	if (runInDisassemblyMode)
	{
		std::pair<EDisassemblyResult, std::string> disassemblyResult = writeDisassembledData(ROMPath, readBufferSize, programStartAddress);
		if (disassemblyResult.first != EDisassemblyResult::Success)
		{
			Logger::logErrorMessage(getDisassemblyErrorMessage(disassemblyResult));
			return static_cast<int>(disassemblyResult.first);
		}
	}
	else
	{
		std::pair<EEmulationResult, std::string> emulationResult = emulateROM(ROMPath, readBufferSize, programStartAddress);
		if (emulationResult.first != EEmulationResult::Success)
		{
			Logger::logErrorMessage(getEmulationErrorMessage(emulationResult));
			return static_cast<int>(emulationResult.first);
		}
	}
	
	return 0;
}
