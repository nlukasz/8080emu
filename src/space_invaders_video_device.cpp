#include "space_invaders_video_device.hpp"
#include "logger.hpp"

#ifdef EMULATION_TURBO_MODE
const float SpaceInvadersVideoDevice::INTERRUPT_TIME = 1.0f / 240.0f;
#else
const float SpaceInvadersVideoDevice::INTERRUPT_TIME = 1.0f / 120.0f;
#endif

SpaceInvadersVideoDevice::~SpaceInvadersVideoDevice()
{
	closeSDL();
}

ExternalDeviceSettings SpaceInvadersVideoDevice::getVideoDeviceSettings()
{
	return ExternalDeviceSettings(true, false, false, { SpaceInvadersInputPorts::INP0, SpaceInvadersInputPorts::INP1, SpaceInvadersInputPorts::INP2 }, false, { SpaceInvadersAudioPorts::SOUND1, SpaceInvadersAudioPorts::SOUND2 }, true);
}

void SpaceInvadersVideoDevice::doExternalDeviceInitialization()
{
	readBytes[SpaceInvadersInputPorts::INP0] = 0x0E;
	readBytes[SpaceInvadersInputPorts::INP1] = 0x08;
	readBytes[SpaceInvadersInputPorts::INP2] = 0x00;
	
	initSDL();
	initSounds();
}

uint32_t SpaceInvadersVideoDevice::mainThreadDeviceUpdate(const float inTimeSinceLastUpdate)
{
	timeSinceLastInterrupt += inTimeSinceLastUpdate;
	if (!SDLInitialized)
	{
		return 1;
	}
	if (quitRequested)
	{
		return 2;
	}
	if (timeSinceLastInterrupt >= INTERRUPT_TIME)
	{
		timeSinceLastInterrupt = timeSinceLastInterrupt - INTERRUPT_TIME;
		interruptInstructionByte = endOfTheFrame ? 0xD7 : 0xCF;
		if (endOfTheFrame)
		{
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
			SDL_RenderClear(renderer);
			drawScreen(0, 96);
		}
		else
		{
			drawScreen(96, SCREEN_HEIGHT);
			SDL_RenderPresent(renderer);
		}
		endOfTheFrame = !endOfTheFrame;
		generateInterrupt = true;
	}
	updateEvents();
	return 0;
}

void SpaceInvadersVideoDevice::handleByteWriteAtPort(const uint8_t inPort)
{
	uint8_t soundByte = 0;
	if (!getWriteByteAtPort(inPort, soundByte))
	{
		return;
	}
	playSound(soundByte, inPort == SpaceInvadersAudioPorts::SOUND1 ? sounds1Map : sounds2Map);
}

void SpaceInvadersVideoDevice::initSDL()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		return;
	}
	SDLInitialized = true;
	
	if (SDL_CreateWindowAndRenderer(SCREEN_HEIGHT, SCREEN_WIDTH, SDL_WINDOW_SHOWN, &window, &renderer) != 0)
	{
		return;
	}
	SDL_SetWindowTitle(window, "Space Invaders");
	
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		return;
	}
	SDLMixerInitialized = true;
	
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);
	updateEvents();
}

void SpaceInvadersVideoDevice::initSounds()
{
	std::unordered_map<uint8_t, std::string> sounds1NamesMap;
	std::unordered_map<uint8_t, std::string> sounds2NamesMap;
	
	sounds1NamesMap[0] = "ufo_highpitch.wav";
	sounds1NamesMap[1] = "shoot.wav";
	sounds1NamesMap[2] = "explosion.wav";
	sounds1NamesMap[3] = "invaderkilled.wav";
	//sounds1Map[4] = ".wav";
	//sounds1Map[5] = ".wav";
	
	sounds2NamesMap[0] = "fastinvader1.wav";
	sounds2NamesMap[1] = "fastinvader2.wav";
	sounds2NamesMap[2] = "fastinvader3.wav";
	sounds2NamesMap[3] = "fastinvader4.wav";
	sounds2NamesMap[4] = "ufo_lowpitch.wav";
	
	const std::string soundsFolderPath = "sounds";

	for (auto it = sounds1NamesMap.begin(); it != sounds1NamesMap.end(); ++it)
	{
		Mix_Chunk* sound = Mix_LoadWAV(std::string(soundsFolderPath + "/" + it->second).c_str());
		if (sound != nullptr)
		{
			SoundData soundData;
			soundData.sound = sound;
			soundData.repeated = it->first == 0;
			sounds1Map[it->first] = soundData;
		}
	}
	
	for (auto it = sounds2NamesMap.begin(); it != sounds2NamesMap.end(); ++it)
	{
		Mix_Chunk* sound = Mix_LoadWAV(std::string(soundsFolderPath + "/" + it->second).c_str());
		if (sound != nullptr)
		{
			SoundData soundData;
			soundData.sound = sound;
			sounds2Map[it->first] = soundData;
		}
	}
}

void SpaceInvadersVideoDevice::closeSDL()
{
	if (window != nullptr)
	{
		SDL_DestroyRenderer(renderer);
		renderer = nullptr;
		SDL_DestroyWindow(window);
		window = nullptr;
	}
	if (SDLMixerInitialized)
	{
		for (auto it = sounds1Map.begin(); it != sounds1Map.end(); ++it)
		{
			Mix_FreeChunk(it->second.sound);
		}
		sounds1Map.clear();
		for (auto it = sounds2Map.begin(); it != sounds2Map.end(); ++it)
		{
			Mix_FreeChunk(it->second.sound);
		}
		sounds2Map.clear();
		
		Mix_Quit();
		SDLMixerInitialized = false;
	}
	if (SDLInitialized)
	{
		SDL_Quit();
		SDLInitialized = false;
	}
}

void SpaceInvadersVideoDevice::updateEvents()
{
	SDL_Event event;
	if (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
		{
			quitRequested = true;
		}
		else if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
				case SDLK_LEFT:
					readBytes[SpaceInvadersInputPorts::INP1] |= 0x20;
					break;
				case SDLK_RIGHT:
					readBytes[SpaceInvadersInputPorts::INP1] |= 0x40;
					break;
				case SDLK_UP:
					readBytes[SpaceInvadersInputPorts::INP1] |= 0x10;
					break;
				case SDLK_z:
					readBytes[SpaceInvadersInputPorts::INP1] |= 0x04;
					break;
				case SDLK_x:
					readBytes[SpaceInvadersInputPorts::INP1] |= 0x01;
					break;
			}
		}
		else if (event.type == SDL_KEYUP)
		{
			switch (event.key.keysym.sym)
			{
				case SDLK_LEFT:
					readBytes[SpaceInvadersInputPorts::INP1] &= ~(0x20);
					break;
				case SDLK_RIGHT:
					readBytes[SpaceInvadersInputPorts::INP1] &= ~(0x40);
					break;
				case SDLK_UP:
					readBytes[SpaceInvadersInputPorts::INP1] &= ~(0x10);
					break;
				case SDLK_z:
					readBytes[SpaceInvadersInputPorts::INP1] &= ~(0x04);
					break;
				case SDLK_x:
					readBytes[SpaceInvadersInputPorts::INP1] &= ~(0x01);
					break;
			}
		}
	}
}

void SpaceInvadersVideoDevice::setPixelColour(const unsigned int& inX, const unsigned int& inY) const
{
	if ((inY >= 32) && (inY < 64))
	{
		SDL_SetRenderDrawColor(renderer, 0xF8, 0x3B, 0x3A, 255);
	}
	else if ((inY >= 184) && ((inY < 240) || ((inX >= 25) && (inX < 136))))
	{
		SDL_SetRenderDrawColor(renderer, 0x62, 0xDE, 0x6D, 255);
	}
	else
	{
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	}
}

void SpaceInvadersVideoDevice::drawScreen(unsigned int inStartLine, unsigned int inEndLine)
{
	const unsigned int bytesPerRow = SCREEN_WIDTH / 8;
	uint16_t currentMemoryAddress = 0;
	uint8_t screenByte = 0;
	unsigned int currentPixelX = 0;
	unsigned int currentPixelY = 0;
	unsigned int screenY = 0;
	const uint8_t bitsMask = 0x01;
	unsigned int LAST_PIXEL_HEIGHT = SCREEN_WIDTH - 1;
	for (unsigned i = inStartLine; i < inEndLine; ++i)
	{
		for (unsigned int j = 0; j < bytesPerRow; ++j)
		{
			currentMemoryAddress = SCREEN_STARTING_ADDRESS + (i * bytesPerRow) + j;
			if (!memory.getByteAtAddress(currentMemoryAddress, screenByte))
			{
				return;
			}
			currentPixelX = j * 8;
			currentPixelY = i;
			for (unsigned k = 0; k < 8; ++k)
			{
				if ((screenByte & (bitsMask << k)) > 0)
				{
					screenY = LAST_PIXEL_HEIGHT - (currentPixelX + k);
					setPixelColour(currentPixelY, screenY);
					SDL_RenderDrawPoint(renderer, currentPixelY, screenY);
				}
			}
		}
	}
}

void SpaceInvadersVideoDevice::playSound(const uint8_t inSoundByte, std::unordered_map<uint8_t, SoundData>& soundsMap)
{
	const uint8_t bitsMask = 1;
	for (uint8_t i = 0; i < 5; ++i)
	{
		auto iterator = soundsMap.find(i);
		if (iterator == soundsMap.end())
		{
			continue;
		}
		SoundData& soundData = soundsMap[iterator->first];
		if ((inSoundByte & (bitsMask << i)) != 0)
		{
			if (soundData.currentChannel < 0)
			{
				soundData.currentChannel = Mix_PlayChannel(-1, soundData.sound, soundData.repeated ? -1 : 0);
			}
		}
		else
		{
			if (soundData.repeated && (soundData.currentChannel >= 0))
			{
				Mix_HaltChannel(soundData.currentChannel);
			}
			soundData.currentChannel = -1;
		}
	}
}
