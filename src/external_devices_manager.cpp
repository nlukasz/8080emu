#include "external_devices_manager.hpp"
// TODO: remove includes when proper library loading functionality is implemented
#include "space_invaders_shift_device.hpp"
#include "dummy_external_device.hpp"
#include "space_invaders_video_device.hpp"

ExternalDeviceManager::~ExternalDeviceManager()
{
	stopExternalDevices();
}

bool ExternalDeviceManager::setupConnectionsInProcessorAndStartDevices(Processor& inProcessor)
{
#ifndef CPU_DIAG_MODE_ENABLED
	if (!initializeExternalDevices(inProcessor))
	{
		return false;
	}
	
	for (unsigned int i = 0; i < externalDevices.size(); ++i)
	{
		ExternalDevice* externalDevice = externalDevices[i];
		// Read connections
		const std::vector<std::pair<uint8_t, uint8_t>>& readConnections = readConnectionsMapping[externalDevice];
		for (unsigned int j = 0; j < readConnections.size(); ++j)
		{
			const std::pair<uint8_t, uint8_t>& mapping = readConnections[j];
			inProcessor.addInputDeviceConnection(mapping.first, InputDeviceConnection(*externalDevice, mapping.second));
		}
		// Write connections
		const std::vector<std::pair<uint8_t, uint8_t>>& writeConnections = writeConnectionsMapping[externalDevice];
		for (unsigned int j = 0; j < writeConnections.size(); ++j)
		{
			const std::pair<uint8_t, uint8_t>& mapping = writeConnections[j];
			inProcessor.addOutputDeviceConnection(mapping.first, OutputDeviceConnection(*externalDevice, mapping.second));
		}
		externalDevice->startExternalDevice();
	}
#endif
	
	return true;
}

void ExternalDeviceManager::stopExternalDevices()
{
	if (externalDevices.size() > 0)
	{
		for (unsigned int i = static_cast<unsigned int>(externalDevices.size()); i > 0; --i)
		{
			removeExternalDevice(i - 1);
		}
	}
}

bool ExternalDeviceManager::consumeExternalDevicesInterrupts(uint8_t& outIntructionByte)
{
	for (ExternalDevice* externalDevice : devicesGeneratingInterrupts)
	{
		if (externalDevice->consumeInterrupt(outIntructionByte))
		{
			return true;
		}
	}
	return false;
}

std::pair<uint32_t, const ExternalDevice*> ExternalDeviceManager::updateExternalDevicesMainThreadLoop()
{
	for (ExternalDevice* externalDevice : devicesHavingMainThreadLoop)
	{
		uint32_t updateResult = externalDevice->performMainThreadUpdate();
		if (updateResult != 0)
		{
			return std::pair<uint32_t, const ExternalDevice*>(updateResult, externalDevice);
		}
	}
	return std::pair<uint32_t, const ExternalDevice*>(0, nullptr);
}

void ExternalDeviceManager::removeExternalDevice(const unsigned int inIndex)
{
	ExternalDevice* deviceToRemove = externalDevices[inIndex];
	deviceToRemove->stopExternalDevice();
	readConnectionsMapping.erase(deviceToRemove);
	writeConnectionsMapping.erase(deviceToRemove);
	devicesGeneratingInterrupts.erase(deviceToRemove);
	devicesHavingMainThreadLoop.erase(deviceToRemove);
	externalDevices.erase(externalDevices.begin() + inIndex);
}

void ExternalDeviceManager::loadExternalDevicesConfiguration(std::vector<std::string>& outDevicesLibraries, std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& outReadConnectionsMapping, std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& outWriteConnectionsMapping, std::unordered_set<std::string>& outDevicesGeneratingInterrupts)
{
	// TODO implement proper configuration file loading
	// Placeholder: return name of Space Invaders external devices libraries and mapping
	outDevicesLibraries.push_back("space_invaders_shift_device");
	
	std::vector<std::pair<uint8_t, uint8_t>> spaceInvadersShiftDeviceReadMappings;
	spaceInvadersShiftDeviceReadMappings.push_back(std::pair<uint8_t, uint8_t>(3, 0));
	outReadConnectionsMapping.emplace("space_invaders_shift_device", spaceInvadersShiftDeviceReadMappings);
	
	std::vector<std::pair<uint8_t, uint8_t>> spaceInvadersShiftDeviceWriteMappings;
	spaceInvadersShiftDeviceWriteMappings.push_back(std::pair<uint8_t, uint8_t>(4, 0));
	spaceInvadersShiftDeviceWriteMappings.push_back(std::pair<uint8_t, uint8_t>(2, 1));
	outWriteConnectionsMapping.emplace("space_invaders_shift_device", spaceInvadersShiftDeviceWriteMappings);
	
	outDevicesLibraries.push_back("watchdog");
	std::vector<std::pair<uint8_t, uint8_t>> watchdogWriteMappings;
	watchdogWriteMappings.push_back(std::pair<uint8_t, uint8_t>(6, 0));
	outWriteConnectionsMapping.emplace("watchdog", watchdogWriteMappings);
	
	outDevicesLibraries.push_back("space_invaders_video_device");
	std::vector<std::pair<uint8_t, uint8_t>> spaceInvadersVideoDeviceReadMappings;
	spaceInvadersVideoDeviceReadMappings.push_back(std::pair<uint8_t, uint8_t>(0, 0));
	spaceInvadersVideoDeviceReadMappings.push_back(std::pair<uint8_t, uint8_t>(1, 1));
	spaceInvadersVideoDeviceReadMappings.push_back(std::pair<uint8_t, uint8_t>(2, 2));
	outReadConnectionsMapping.emplace("space_invaders_video_device", spaceInvadersVideoDeviceReadMappings);
	std::vector<std::pair<uint8_t, uint8_t>> spaceInvadersSoundDeviceWriteMappings;
	spaceInvadersSoundDeviceWriteMappings.push_back(std::pair<uint8_t, uint8_t>(3, 0));
	spaceInvadersSoundDeviceWriteMappings.push_back(std::pair<uint8_t, uint8_t>(5, 1));
	outWriteConnectionsMapping.emplace("space_invaders_video_device", spaceInvadersSoundDeviceWriteMappings);
	outDevicesGeneratingInterrupts.insert("space_invaders_video_device");
}

bool ExternalDeviceManager::validateMappings(const std::string inDeviceLibrary, const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inConnectionsMapping, std::unordered_set<uint8_t>& inUsedProcessorPorts, std::unordered_set<uint8_t>& inUsedDevicePorts) const
{
	auto connectionsIterator = inConnectionsMapping.find(inDeviceLibrary);
	if (connectionsIterator == inConnectionsMapping.end())
	{
		return true;
	}
	const std::vector<std::pair<uint8_t, uint8_t>>& connections = connectionsIterator->second;
	for (int i = 0; i < connections.size(); ++i)
	{
		const std::pair<uint8_t, uint8_t>& mapping = connections[i];
		// Check if given processor port is not already in use
		auto processorPortsIterator = inUsedProcessorPorts.find(mapping.first);
		if (processorPortsIterator != inUsedProcessorPorts.end())
		{
			return false;
		}
		// Check if given device port is not already in use
		auto devicePortsIterator = inUsedDevicePorts.find(mapping.second);
		if (devicePortsIterator != inUsedDevicePorts.end())
		{
			return false;
		}
		// Add used ports
		inUsedProcessorPorts.emplace(mapping.first);
		inUsedDevicePorts.emplace(mapping.second);
	}
	return true;
}

bool ExternalDeviceManager::validateConnectionsMappings(const std::vector<std::string>& inDevicesLibraries, const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inReadConnectionsMapping, const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inWriteConnectionsMapping, const std::unordered_set<std::string>& inDevicesGeneratingInterrupts) const
{
	std::unordered_set<uint8_t> usedReadProcessorPorts;
	std::unordered_set<uint8_t> usedWriteProcessorPorts;
	
	std::string deviceLibrary;
	std::unordered_set<uint8_t> usedReadDevicePorts;
	std::unordered_set<uint8_t> usedWriteDevicePorts;
	for (unsigned int i = 0; i < inDevicesLibraries.size(); ++i)
	{
		deviceLibrary = inDevicesLibraries[i];
		usedReadDevicePorts.clear();
		usedWriteDevicePorts.clear();
		if (!validateMappings(deviceLibrary, inReadConnectionsMapping, usedReadProcessorPorts, usedReadDevicePorts))
		{
			return false;
		}
		if (!validateMappings(deviceLibrary, inWriteConnectionsMapping, usedWriteProcessorPorts, usedWriteDevicePorts))
		{
			return false;
		}
		// TODO: Check if device is used at all
		if ((usedReadDevicePorts.size() == 0) && (usedWriteDevicePorts.size() == 0) && (inDevicesGeneratingInterrupts.find(deviceLibrary) == inDevicesGeneratingInterrupts.end()))
		{
			return false;
		}
	}
	
	return true;
}

bool ExternalDeviceManager::constructExternaDevice(const std::string inDeviceLibrary, const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inReadConnectionsMapping, const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inWriteConnectionsMapping, const std::unordered_set<std::string>& inDevicesGeneratingInterrupts, const Processor& inProcessor)
{
	// TODO implement proper construction of external device object from library
	// Placeholder: fixed construction of Space Invaders devices objects
	ExternalDevice* newExternalDevice = nullptr;
	
	auto readConnectionsIterator = inReadConnectionsMapping.find(inDeviceLibrary);
	auto writeConnectionsIterator = inWriteConnectionsMapping.find(inDeviceLibrary);
	
	if (inDeviceLibrary == "space_invaders_shift_device")
	{
		newExternalDevice = new SpaceInvadersShiftDevice();
	}
	else if (inDeviceLibrary == "watchdog")
	{
		newExternalDevice = new DummyExternalDevice();
	}
	else if (inDeviceLibrary == "space_invaders_video_device")
	{
		newExternalDevice = new SpaceInvadersVideoDevice(inProcessor.getMemory());
	}
	else
	{
		return false;
	}
	newExternalDevice->initializeExternalDevice();
	
	externalDevices.push_back(newExternalDevice);
	
	if (readConnectionsIterator != inReadConnectionsMapping.end())
	{
		readConnectionsMapping.emplace(newExternalDevice, readConnectionsIterator->second);
	}
	if (writeConnectionsIterator != inWriteConnectionsMapping.end())
	{
		writeConnectionsMapping.emplace(newExternalDevice, writeConnectionsIterator->second);
	}
	if (inDevicesGeneratingInterrupts.find(inDeviceLibrary) != inDevicesGeneratingInterrupts.end())
	{
		devicesGeneratingInterrupts.insert(newExternalDevice);
	}
	if (newExternalDevice->getExternalDeviceSettings().usesMainThreadLoop)
	{
		devicesHavingMainThreadLoop.insert(newExternalDevice);
	}
	
	return true;
}

bool ExternalDeviceManager::initializeExternalDevices(const Processor& inProcessor)
{
	if (externalDevices.size() > 0)
	{
		return false;
	}
	
	std::vector<std::string> devicesLibraries;
	std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>> librariesReadConnectionsMapping;
	std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>> librariesWriteConnectionsMapping;
	std::unordered_set<std::string> devicesGeneratingInterrupts;
	
	loadExternalDevicesConfiguration(devicesLibraries, librariesReadConnectionsMapping, librariesWriteConnectionsMapping, devicesGeneratingInterrupts);
	if (!validateConnectionsMappings(devicesLibraries, librariesReadConnectionsMapping, librariesWriteConnectionsMapping, devicesGeneratingInterrupts))
	{
		return false;
	}
	
	for (unsigned int i = 0; i < devicesLibraries.size(); ++i)
	{
		if (!constructExternaDevice(devicesLibraries[i], librariesReadConnectionsMapping, librariesWriteConnectionsMapping, devicesGeneratingInterrupts, inProcessor))
		{
			return false;
		}
	}
	
	return true;
}
