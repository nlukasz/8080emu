#ifndef instruction_data_hpp
#define instruction_data_hpp

#include <string>
#include <unordered_map>
#include <vector>
#include <utility>
#include <functional>
#include "processor.hpp"

enum class EInstructionExecutionResult
{
	Success = 0,
	KeepProgramCounter,
	KeepProgramCounterAndWaitMaxStatesCount,
	InstructionNotImplemented,
	ExecutionError
};

class CPUInstruction
{
public:
	CPUInstruction(std::string inReadableName, unsigned int inStatesCount, std::function<EInstructionExecutionResult(Processor&, const CPUInstruction&)> inImplementationFunction = nullptr, unsigned int inParameterBytesCount = 0, int inMaximumStatesCount = -1)
	: readableName(inReadableName), statesCount(inStatesCount), implementationFunction(inImplementationFunction), maximumStatesCount(inMaximumStatesCount)
	{
		parameterBytes.resize(inParameterBytesCount);
	}
	
	std::string getReadableName() const
	{
		return readableName;
	}
	
	unsigned int getStatesCount() const
	{
		return statesCount;
	}
	
	bool getArgumentValue(unsigned int inArgumentIndex, uint8_t& outArgumentValue) const
	{
		if (inArgumentIndex >= static_cast<unsigned int>(parameterBytes.size()))
		{
			return false;
		}
		outArgumentValue = parameterBytes[inArgumentIndex];
		return true;
	}
	
	bool setArgumentValue(unsigned int inArgumentIndex, const uint8_t inArgumentValue)
	{
		if (inArgumentIndex >= static_cast<unsigned int>(parameterBytes.size()))
		{
			return false;
		}
		parameterBytes[inArgumentIndex] = inArgumentValue;
		return true;
	}
	
	unsigned int getArgumentsBytesCount() const
	{
		return static_cast<unsigned int>(parameterBytes.size());
	}
	
	EInstructionExecutionResult executeInstruction(Processor& processor) const
	{
		if (implementationFunction == nullptr)
		{
			return EInstructionExecutionResult::InstructionNotImplemented;
		}
		
		return implementationFunction(processor, *this);
	}
	
	unsigned int getMaximumStatesCount() const
	{
		return (maximumStatesCount > 0 ? maximumStatesCount : statesCount);
	}
	
private:
	std::string readableName = "";
	unsigned int statesCount = 0;
	std::vector<uint8_t> parameterBytes;
	std::function<EInstructionExecutionResult(Processor&, const CPUInstruction&)> implementationFunction;
	int maximumStatesCount = -1;
};

class MemoryCPUInstruction : public CPUInstruction
{
public:
	MemoryCPUInstruction(std::string inReadableName, unsigned int inStatesCount, std::function<EInstructionExecutionResult(Processor&, const CPUInstruction&)> inImplementationFunction = nullptr, int inMaximumStatesCount = -1)
	: CPUInstruction(inReadableName, inStatesCount, inImplementationFunction, 2, inMaximumStatesCount) {}
};

std::unordered_map<uint8_t, CPUInstruction> getCPUInstructions();

#endif /* instruction_data_hpp */
