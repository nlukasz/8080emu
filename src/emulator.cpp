#include <sstream>
#include <chrono>
#include <thread>
#include "emulator.hpp"
#include "printing_functions.hpp"
#include "external_devices_manager.hpp"
#include "logger.hpp"

EEmulationResult loadProgramIntoMemory(std::string inROMFilePath, unsigned int inReadBufferSize, Processor& inProcessor, unsigned int& outROMFileSize, const unsigned int startingMemoryAddress = 0)
{
	EEmulationResult result = EEmulationResult::Success;
	
	RomFileReader romFileReader(inROMFilePath, inReadBufferSize);
	if (!romFileReader.openROMFile())
	{
		result = EEmulationResult::CantOpenROMFile;
		return result;
	}
	
	uint8_t currentByte = '\0';
	unsigned int currentMemoryIndex = startingMemoryAddress;
	while (romFileReader.getNextByte(currentByte))
	{
		if (!inProcessor.setByteInMemory(currentMemoryIndex, currentByte))
		{
			result = EEmulationResult::ProgramBiggerThanAvailableMemory;
			break;
		}
		++currentMemoryIndex;
	}
	if (!romFileReader.isEOF())
	{
		result = EEmulationResult::FileReadError;
	}
	outROMFileSize = romFileReader.getCurrentlyOpenedROMFileSize();
	
	return result;
}

void performEmulationLoop(Processor& inProcessor, ExternalDeviceManager& inExternalDevicesManager, std::pair<EEmulationResult, std::string>& outEmulationResult, bool& outProgramFinished, const unsigned int inRomFileSize = 0)
{
	float cpuStateTime = 1.0f / static_cast<float>(inProcessor.CPU_FREQUENCY);
	uint8_t currentInstructionByte = 0;
	bool handlesInterrupt = false;
	outProgramFinished = false;
	std::unordered_map<uint8_t, CPUInstruction> cpuInstructions = getCPUInstructions();
	
	while (!outProgramFinished)
	{
		if (inProcessor.areInterruptsEnabled() && inExternalDevicesManager.consumeExternalDevicesInterrupts(currentInstructionByte))
		{
			handlesInterrupt = true;
			inProcessor.setInterruptsEnabled(false);
			inProcessor.setHalted(false);
		}
		else if (inProcessor.isHalted())
		{
			continue;
		}
		else if (!inProcessor.getByteAtProgramCounter(currentInstructionByte))
		{
			outEmulationResult.first = EEmulationResult::ProgramDataReadError;
			break;
		}
		auto foundInstruction = cpuInstructions.find(currentInstructionByte);
		if (foundInstruction == cpuInstructions.end())
		{
			Logger::logErrorMessage("Unrecognized instruction: " + std::to_string(currentInstructionByte));
			outEmulationResult.first = EEmulationResult::UnrecognizedCPUInstruction;
			outEmulationResult.second = std::to_string(currentInstructionByte);
			break;
		}
		CPUInstruction cpuInstruction = foundInstruction->second;
		
		//float currentInstructionDuration = cpuStateTime * cpuInstruction.getStatesCount();
#ifndef EMULATION_TURBO_MODE
		std::chrono::duration<float> currentInstructionDuration = std::chrono::duration<float>(cpuStateTime * cpuInstruction.getStatesCount());
		std::chrono::time_point<std::chrono::steady_clock> instructionStartTime = std::chrono::steady_clock::now();
#ifdef DEBUG_MESSAGES_ENABLED
		Logger::logMessage("Instruction duration: " + std::to_string(currentInstructionDuration.count()));
#endif
#endif
		
#ifdef DEBUG_MESSAGES_ENABLED
		inProcessor.printCPUState();
#endif
		if (!handlesInterrupt)
		{
			for (int i = 0; i < cpuInstruction.getArgumentsBytesCount(); ++i)
			{
				inProcessor.incraseProgramCounterBy();
				uint8_t argumentValue = 0;
				if (!inProcessor.getByteAtProgramCounter(argumentValue))
				{
					outEmulationResult.first = EEmulationResult::ProgramDataReadError;
					break;
				}
				if (!cpuInstruction.setArgumentValue(i, argumentValue))
				{
					outEmulationResult.first = EEmulationResult::WrongCPUInstructionArgument;
					break;
				}
			}
		}
		
#ifdef DEBUG_MESSAGES_ENABLED
		{
			std::stringstream emulatedInstructionStringStream;
			emulatedInstructionStringStream << "Emulating instruction: ";
			emulatedInstructionStringStream << getInstructionString(cpuInstruction);
			Logger::logMessage(emulatedInstructionStringStream.str());
		}
#endif
		
		EInstructionExecutionResult instructionResult = cpuInstruction.executeInstruction(inProcessor);
		if (instructionResult == EInstructionExecutionResult::InstructionNotImplemented)
		{
			outEmulationResult.first = EEmulationResult::UnimplementedInstruction;
			outEmulationResult.second = cpuInstruction.getReadableName();
			break;
		}
		else if (instructionResult == EInstructionExecutionResult::ExecutionError)
		{
			outEmulationResult.first = EEmulationResult::InstructionExecutionError;
			outEmulationResult.second = cpuInstruction.getReadableName();
			break;
		}
#ifndef EMULATION_TURBO_MODE
		else if (instructionResult == EInstructionExecutionResult::KeepProgramCounterAndWaitMaxStatesCount)
		{
			currentInstructionDuration = std::chrono::duration<float>(cpuStateTime * cpuInstruction.getMaximumStatesCount());
#ifdef DEBUG_MESSAGES_ENABLED
			Logger::logMessage("Instruction maximum duration: " + std::to_string(currentInstructionDuration.count()));
#endif
		}
#endif
		else if (!handlesInterrupt && (instructionResult == EInstructionExecutionResult::Success))
		{
			inProcessor.incraseProgramCounterBy();
#ifdef CPU_DIAG_MODE_ENABLED
			if (inProcessor.getProgramCounter() >= inRomFileSize)
			{
#ifdef DEBUG_MESSAGES_ENABLED
				Logger::logMessage("Reached end of the ROM.");
#endif
				outProgramFinished = true;
			}
#endif
		}
		handlesInterrupt = false;
		
#ifndef EMULATION_TURBO_MODE
		std::chrono::time_point<std::chrono::steady_clock> instructionEndTime = std::chrono::steady_clock::now();
		std::chrono::duration<float> elapsedInstructionTime = instructionEndTime - instructionStartTime;
		if (elapsedInstructionTime < currentInstructionDuration)
		{
			std::this_thread::sleep_for(currentInstructionDuration - elapsedInstructionTime);
		}
		
#ifdef DEBUG_MESSAGES_ENABLED
		instructionEndTime = std::chrono::steady_clock::now();
		elapsedInstructionTime = instructionEndTime - instructionStartTime;
		Logger::logMessage("Time elapsed: " + std::to_string(elapsedInstructionTime.count()));
#endif
#endif
	}
	outProgramFinished = true;
}

std::pair<EEmulationResult, std::string> emulateROM(std::string inROMFilePath, unsigned int inReadBufferSize, const uint16_t inProgramStartAddress)
{
	std::pair<EEmulationResult, std::string> emulationResult = std::pair<EEmulationResult, std::string>(EEmulationResult::Success, "");
	
	Processor processor(inProgramStartAddress);
	unsigned int romFileSize = 0;
	emulationResult.first = loadProgramIntoMemory(inROMFilePath, inReadBufferSize, processor, romFileSize, inProgramStartAddress);
	if (emulationResult.first != EEmulationResult::Success)
	{
		return;
	}
	
	ExternalDeviceManager externalDevicesManager;
	if (!externalDevicesManager.setupConnectionsInProcessorAndStartDevices(processor))
	{
		emulationResult.first = EEmulationResult::ExternalDevicesSetupFailed;
		return;
	}
	
	bool programFinished = false;
	std::pair<uint32_t, const ExternalDevice*> externalDeviceMainThreadLoopUpdateResult;
	std::thread emulationThread(performEmulationLoop, std::ref(processor), std::ref(externalDevicesManager), std::ref(emulationResult), std::ref(programFinished), (romFileSize + inProgramStartAddress));
	while (!programFinished)
	{
		externalDeviceMainThreadLoopUpdateResult = externalDevicesManager.updateExternalDevicesMainThreadLoop();
		if (externalDeviceMainThreadLoopUpdateResult.first > 0)
		{
			// TODO: error message from external device
			emulationResult.first = EEmulationResult::ExternalDeviceMainThreadUpdateError;
			programFinished = true;
			break;
		}
	}
	emulationThread.join();
	externalDevicesManager.stopExternalDevices();
	return emulationResult;
}

std::pair<EEmulationResult, std::string> emulateROMInThread(std::string inROMFilePath, unsigned int inReadBufferSize, const uint16_t inProgramStartAddress)
{
	std::pair<EEmulationResult, std::string> emulationResult;
	return emulationResult;
}

std::string getEmulationErrorMessage(const std::pair<EEmulationResult, std::string>& emulationResult)
{
	switch(emulationResult.first)
	{
		case EEmulationResult::CantOpenROMFile:
			return "Can't open ROM file!";
		case EEmulationResult::FileReadError:
			return "File read error!";
		case EEmulationResult::UnrecognizedCPUInstruction:
			return std::string("Unrecognized CPU instruction: ") + emulationResult.second;
		case EEmulationResult::WrongCPUInstructionArgument:
			return "Wrong CPU instruction argument!";
		case EEmulationResult::UnimplementedInstruction:
			return std::string("Unimplemented CPU instruction: ") + emulationResult.second;
		case EEmulationResult::InstructionExecutionError:
			return std::string("CPU instruction execution failed: ") + emulationResult.second;
		case EEmulationResult::ProgramDataReadError:
			return "Program data read error!";
		case EEmulationResult::ProgramBiggerThanAvailableMemory:
			return "Program size is bigger than available emulated memory!";
		case EEmulationResult::ExternalDevicesSetupFailed:
			return "Setup of external devices failed!";
		case EEmulationResult::ExternalDeviceMainThreadUpdateError:
			return std::string("External device main thread loop update error: ") + emulationResult.second;
		default:
			return "Unknown error!";
	}
}
