#ifndef byte_functions_hpp
#define byte_functions_hpp

#include <cinttypes>

uint16_t combineTwoBytesToWord(const uint8_t inHigherByte, const uint8_t inLowerByte);

void splitWordToTwoBytes(uint8_t& inHigherByte, uint8_t& inLowerByte, const uint16_t inWord);

bool isNumberOfSetBitsInByteEven(const uint8_t inByte);

void setBitInByte(const bool inNewBitState, const uint8_t inBitMask, uint8_t& outByte);

#endif /* byte_functions_hpp */
