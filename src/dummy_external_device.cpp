#include "dummy_external_device.hpp"

ExternalDeviceSettings DummyExternalDevice::getDummyDeviceSettings()
{
	return ExternalDeviceSettings(false, false, true, {}, true, {}, false);
}

void DummyExternalDevice::doExternalDeviceInitialization()
{
	for (unsigned int i = 0; i < 256; ++i)
	{
		readBytes[i] = 0;
	}
}
