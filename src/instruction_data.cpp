#include "instruction_data.hpp"
#include "byte_functions.hpp"
#ifdef CPU_DIAG_MODE_ENABLED
#include <string>
#include "logger.hpp"
#endif

inline void addToAccumulator(Processor& inProcessor, const uint8_t inAddedValue)
{
	uint16_t result = static_cast<uint16_t>(inProcessor.A) + static_cast<uint16_t>(inAddedValue);
	inProcessor.Flags.C = (result > 0xFF);
	inProcessor.Flags.AC = ((inProcessor.A & 0x0F) + (inAddedValue & 0x0F)) > 0x0F;
	inProcessor.A = result & 0xFF;
	inProcessor.updateFlagsForAccumulator();
}

inline void subtractFromAccumulator(Processor& inProcessor, const uint8_t inSubtractedValue)
{
	uint16_t complementarySubtractedValue = static_cast<uint16_t>(~inSubtractedValue);
	uint16_t result = static_cast<uint16_t>(inProcessor.A) + complementarySubtractedValue + 1;
	inProcessor.Flags.C = (result > 0xFF);
	inProcessor.Flags.AC = ((inProcessor.A & 0x0F) + static_cast<uint8_t>(complementarySubtractedValue & 0x0F) + 1) > 0x0F;
	inProcessor.A = result & 0xFF;
	inProcessor.updateFlagsForAccumulator();
}

inline void incrementRegisterPairValue(uint8_t& inFirstRegister, uint8_t& inSecondRegister)
{
	++inSecondRegister;
	if (inSecondRegister == 0)
	{
		++inFirstRegister;
	}
}

inline void decrementRegisterPairValue(uint8_t& inFirstRegister, uint8_t& inSecondRegister)
{
	--inSecondRegister;
	if (inSecondRegister == 0xFF)
	{
		--inFirstRegister;
	}
}

inline void addRegisterPairToHL(Processor& inProcessor, uint16_t inRegisterPairValue)
{
	uint16_t HLValue = inProcessor.getHLPairValue();
	unsigned int result = static_cast<unsigned int>(HLValue) + static_cast<unsigned int>(inRegisterPairValue);
	inProcessor.Flags.C = result > 0xFFFF;
	inProcessor.setHLPairValue(static_cast<uint16_t>(result & 0xFFFF));
}

inline void incrementRegister(Processor& inProcessor, uint8_t& inRegister)
{
	inProcessor.Flags.AC = ((inRegister & 0x0F) + 1) > 0x0F;
	++inRegister;
	inProcessor.updateFlagsForByte(inRegister);
}

inline void decrementRegister(Processor& inProcessor, uint8_t& inRegister)
{
	inProcessor.Flags.AC = ((inRegister & 0x0F) + 0x0F) > 0x0F;	// Add (~0x1 + 1) and check if we have carry on 4th bit
	--inRegister;
	inProcessor.updateFlagsForByte(inRegister);
}

inline bool setPCFromArguments(Processor& inProcessor, const CPUInstruction& inInstruction)
{
	uint8_t highByte = 0;
	uint8_t lowByte = 0;
	if (!inInstruction.getArgumentValue(1, highByte))
	{
		return false;
	}
	if (!inInstruction.getArgumentValue(0, lowByte))
	{
		return false;
	}
	inProcessor.setPC(highByte, lowByte);
	return true;
}

inline EInstructionExecutionResult jumpOnCondition(Processor& inProcessor, const CPUInstruction& inInstruction, const bool inJumpCondition)
{
	if (inJumpCondition)
	{
		return (setPCFromArguments(inProcessor, inInstruction) ? EInstructionExecutionResult::KeepProgramCounter : EInstructionExecutionResult::ExecutionError);
	}
	return EInstructionExecutionResult::Success;
}

inline bool executeCall(Processor& inProcessor, const CPUInstruction& inInstruction)
{
	if (!inProcessor.pushOnStack(inProcessor.getProgramCounter() + 1))
	{
		return false;
	}
	return setPCFromArguments(inProcessor, inInstruction);
}

inline EInstructionExecutionResult callOnCondition(Processor& inProcessor, const CPUInstruction& inInstruction, const bool inCallCondition)
{
	if (inCallCondition)
	{
		return (executeCall(inProcessor, inInstruction) ? EInstructionExecutionResult::KeepProgramCounterAndWaitMaxStatesCount : EInstructionExecutionResult::ExecutionError);
	}
	return EInstructionExecutionResult::Success;
}

inline bool executeRet(Processor& inProcessor, const CPUInstruction& inInstruction)
{
	uint16_t retPC = 0;
	if (!inProcessor.popFromStack(retPC))
	{
		return false;
	}
	uint8_t highByte = 0;
	uint8_t lowByte = 0;
	splitWordToTwoBytes(highByte, lowByte, retPC);
	inProcessor.setPC(highByte, lowByte);
	return true;
}

inline EInstructionExecutionResult retOnCondition(Processor& inProcessor, const CPUInstruction& inInstruction, const bool inRetCondition)
{
	if (inRetCondition)
	{
		return (executeRet(inProcessor, inInstruction) ? EInstructionExecutionResult::KeepProgramCounterAndWaitMaxStatesCount : EInstructionExecutionResult::ExecutionError);
	}
	return EInstructionExecutionResult::Success;
}

inline EInstructionExecutionResult performReset(Processor& inProcessor, const uint8_t inValue)
{
	if (!inProcessor.pushOnStack(inProcessor.getProgramCounter()))
	{
		return EInstructionExecutionResult::ExecutionError;
	}
	inProcessor.setHalted(false);
	inProcessor.setPC(0, (inValue << 3));
	return EInstructionExecutionResult::KeepProgramCounter;
}

inline void performANDOperationWithAccumulator(Processor& inProcessor, const uint8_t inSecondNumber)
{
	inProcessor.Flags.AC = ((inProcessor.A | inSecondNumber) & 0x08) != 0;
	inProcessor.A = inProcessor.A & inSecondNumber;
	inProcessor.updateFlagsForAccumulator();
	inProcessor.Flags.C = false;
}

inline void performXOROperationWithAccumulator(Processor& inProcessor, const uint8_t inSecondNumber)
{
	inProcessor.A = inProcessor.A ^ inSecondNumber;
	inProcessor.updateFlagsForAccumulator();
	inProcessor.Flags.C = false;
	inProcessor.Flags.AC = false;
}

inline void performOROperationWithAccumulator(Processor& inProcessor, const uint8_t inSecondNumber)
{
	inProcessor.A = inProcessor.A | inSecondNumber;
	inProcessor.updateFlagsForAccumulator();
	inProcessor.Flags.C = false;
	inProcessor.Flags.AC = false;
}

inline void compareWithAccumulator(Processor& inProcessor, const uint8_t inSecondNumber)
{
	uint16_t complementarySecondNumber = static_cast<uint16_t>(~inSecondNumber);
	uint16_t result = static_cast<uint16_t>(inProcessor.A) + complementarySecondNumber + 1;
	inProcessor.updateFlagsForByte(result & 0xFF);
	inProcessor.Flags.AC = ((inProcessor.A & 0x0F) + static_cast<uint8_t>(complementarySecondNumber & 0x0F) + 1) > 0x0F;
	inProcessor.Flags.C = (result > 0xFF);
}

inline EInstructionExecutionResult moveImmediateValueToByte(uint8_t& outByte, const CPUInstruction& inInstruction)
{
	uint8_t argumentValue = 0;
	if (!inInstruction.getArgumentValue(0, argumentValue))
	{
		return EInstructionExecutionResult::ExecutionError;
	}
	outByte = argumentValue;
	return EInstructionExecutionResult::Success;
}

inline EInstructionExecutionResult loadImmediateValueToBytesPair(uint8_t& outHighByte, uint8_t& outLowByte, const CPUInstruction& inInstruction)
{
	if (!inInstruction.getArgumentValue(0, outLowByte))
	{
		return EInstructionExecutionResult::ExecutionError;
	}
	if (!inInstruction.getArgumentValue(1, outHighByte))
	{
		return EInstructionExecutionResult::ExecutionError;
	}
	return EInstructionExecutionResult::Success;
}

inline EInstructionExecutionResult loadFromMemoryToAccumulator(Processor& inProcessor, const uint8_t inAddressHighByte, const uint8_t inAddressLowByte)
{
	return inProcessor.getByteInMemory(inAddressHighByte, inAddressLowByte, inProcessor.A) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
}

inline EInstructionExecutionResult storeAccumulatorInMemory(Processor& inProcessor, const uint8_t inAddressHighByte, const uint8_t inAddressLowByte)
{
	return inProcessor.setByteInMemory(inAddressHighByte, inAddressLowByte, inProcessor.A) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
}

// Instructions
std::unordered_map<uint8_t, CPUInstruction> getCPUInstructions()
{
	std::unordered_map<uint8_t, CPUInstruction> CPUInstructions;
	
	// Repeated instructions objects
	CPUInstruction nopInstruction("NOP", 4,[](Processor&, const CPUInstruction&)->EInstructionExecutionResult{ return EInstructionExecutionResult::Success; });
	MemoryCPUInstruction jmpInstruction("JMP", 10,
		[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
		{
			return (setPCFromArguments(inProcessor, inInstruction) ? EInstructionExecutionResult::KeepProgramCounter : EInstructionExecutionResult::ExecutionError);
		}
	);
	CPUInstruction retInstruction("RET", 10,
		[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
		{
			return (executeRet(inProcessor, inInstruction) ? EInstructionExecutionResult::KeepProgramCounter : EInstructionExecutionResult::ExecutionError);
		}
	);
	MemoryCPUInstruction callInstruction("CALL", 17,
		[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
		{
#ifdef CPU_DIAG_MODE_ENABLED
			uint8_t lowerByte = 0;
			uint8_t higherByte = 0;
			if (!inInstruction.getArgumentValue(0, lowerByte))
			{
				return EInstructionExecutionResult::ExecutionError;
			}
			if (!inInstruction.getArgumentValue(1, higherByte))
			{
				return EInstructionExecutionResult::ExecutionError;
			}
			uint16_t address = combineTwoBytesToWord(higherByte, lowerByte);
			if ((address == 5) || (address == 1))
			{
				if (inProcessor.C == 9)
				{
					std::string message = "";
					uint16_t nextCharacterAddress = inProcessor.getDEPairValue()/* + 3*/;
					char messageCharacter = '\0';
					const char stoppingCharacter = '$';
					while (messageCharacter != stoppingCharacter)
					{
						uint8_t byteInMemory = 0;
						if (!inProcessor.getByteInMemory(nextCharacterAddress, byteInMemory))
						{
							return EInstructionExecutionResult::ExecutionError;
						}
						messageCharacter = static_cast<char>(byteInMemory);
						if (messageCharacter != stoppingCharacter)
						{
							message += messageCharacter;
							++nextCharacterAddress;
						}
					}
					Logger::logMessage(message, false);
				}
				else if (inProcessor.C == 2)
				{
					Logger::logMessage(std::string(1, static_cast<char>(inProcessor.E)), false);
				}
				else
				{
					Logger::logMessage("?");
				}
				return EInstructionExecutionResult::Success;
			}
			else if (address == 0)
			{
				return EInstructionExecutionResult::ExecutionError;
			}
#endif
			return (executeCall(inProcessor, inInstruction) ? EInstructionExecutionResult::KeepProgramCounter : EInstructionExecutionResult::ExecutionError);
		}
	);
	
	// Instructions
	CPUInstructions.emplace(0x00, nopInstruction);
	CPUInstructions.emplace(0x01, CPUInstruction("LXI B,", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return loadImmediateValueToBytesPair(inProcessor.B, inProcessor.C, inInstruction);
	}, 2));
	CPUInstructions.emplace(0x02, CPUInstruction("STAX B", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return storeAccumulatorInMemory(inProcessor, inProcessor.B, inProcessor.C);
	}));
	CPUInstructions.emplace(0x03, CPUInstruction("INX B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegisterPairValue(inProcessor.B, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x04, CPUInstruction("INR B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegister(inProcessor, inProcessor.B);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x05, CPUInstruction("DCR B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegister(inProcessor, inProcessor.B);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x06, CPUInstruction("MVI B,", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return moveImmediateValueToByte(inProcessor.B, inInstruction);
	}, 1));
	CPUInstructions.emplace(0x07, CPUInstruction("RLC", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		const bool carry = (inProcessor.A & 0x80) != 0;
		inProcessor.A = inProcessor.A << 1;
		inProcessor.A = inProcessor.A | (carry ? 0x01 : 0x00);
		inProcessor.Flags.C = carry;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x09, CPUInstruction("DAD B", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addRegisterPairToHL(inProcessor, inProcessor.getBCPairValue());
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x0A, CPUInstruction("LDAX B", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return loadFromMemoryToAccumulator(inProcessor, inProcessor.B, inProcessor.C);
	}));
	CPUInstructions.emplace(0x0B, CPUInstruction("DCX B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegisterPairValue(inProcessor.B, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x0C, CPUInstruction("INR C", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegister(inProcessor, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x0D, CPUInstruction("DCR C", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegister(inProcessor, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x0E, CPUInstruction("MVI C,", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return moveImmediateValueToByte(inProcessor.C, inInstruction);
	}, 1));
	CPUInstructions.emplace(0x0F, CPUInstruction("RRC", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		const bool carry = (inProcessor.A & 0x01) != 0;
		inProcessor.A = inProcessor.A >> 1;
		inProcessor.A = inProcessor.A | (carry ? 0x80 : 0x00);
		inProcessor.Flags.C = carry;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x11, CPUInstruction("LXI D,", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return loadImmediateValueToBytesPair(inProcessor.D, inProcessor.E, inInstruction);
	}, 2));
	CPUInstructions.emplace(0x12, CPUInstruction("STAX D", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return storeAccumulatorInMemory(inProcessor, inProcessor.D, inProcessor.E);
	}));
	CPUInstructions.emplace(0x13, CPUInstruction("INX D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegisterPairValue(inProcessor.D, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x14, CPUInstruction("INR D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegister(inProcessor, inProcessor.D);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x15, CPUInstruction("DCR D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegister(inProcessor, inProcessor.D);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x16, CPUInstruction("MVI D,", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return moveImmediateValueToByte(inProcessor.D, inInstruction);
	}, 1));
	CPUInstructions.emplace(0x17, CPUInstruction("RAL", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		const bool carry = (inProcessor.A & 0x80) != 0;
		inProcessor.A = inProcessor.A << 1;
		inProcessor.A = inProcessor.A | (inProcessor.Flags.C ? 0x01 : 0x00);
		inProcessor.Flags.C = carry;
		return EInstructionExecutionResult::Success;
	}));

	CPUInstructions.emplace(0x19, CPUInstruction("DAD D", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addRegisterPairToHL(inProcessor, inProcessor.getDEPairValue());
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x1A, CPUInstruction("LDAX D", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return loadFromMemoryToAccumulator(inProcessor, inProcessor.D, inProcessor.E);
	}));
	CPUInstructions.emplace(0x1B, CPUInstruction("DCX D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegisterPairValue(inProcessor.D, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x1C, CPUInstruction("INR E", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegister(inProcessor, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x1D, CPUInstruction("DCR E", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegister(inProcessor, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x1E, CPUInstruction("MVI E,", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return moveImmediateValueToByte(inProcessor.E, inInstruction);
	}, 1));
	CPUInstructions.emplace(0x1F, CPUInstruction("RAR", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		const bool carry = (inProcessor.A & 0x01) != 0;
		inProcessor.A = inProcessor.A >> 1;
		inProcessor.A = inProcessor.A | (inProcessor.Flags.C ? 0x80 : 0x00);
		inProcessor.Flags.C = carry;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x21, CPUInstruction("LXI H,", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return loadImmediateValueToBytesPair(inProcessor.H, inProcessor.L, inInstruction);
	}, 2));
	CPUInstructions.emplace(0x22, MemoryCPUInstruction("SHLD", 16,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t highByte = 0;
		uint8_t lowByte = 0;
		if (!inInstruction.getArgumentValue(1, highByte))
		{
		   return EInstructionExecutionResult::ExecutionError;
		}
		if (!inInstruction.getArgumentValue(0, lowByte))
		{
		   return EInstructionExecutionResult::ExecutionError;
		}
		const uint16_t memoryAddress = combineTwoBytesToWord(highByte, lowByte);
		if (!inProcessor.setByteInMemory(memoryAddress, inProcessor.L))
		{
		   return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.setByteInMemory(memoryAddress + 1, inProcessor.H))
		{
		   return EInstructionExecutionResult::ExecutionError;
		}
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x23, CPUInstruction("INX H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegisterPairValue(inProcessor.H, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x24, CPUInstruction("INR H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegister(inProcessor, inProcessor.H);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x25, CPUInstruction("DCR H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegister(inProcessor, inProcessor.H);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x26, CPUInstruction("MVI H,", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return moveImmediateValueToByte(inProcessor.H, inInstruction);
	}, 1));
	CPUInstructions.emplace(0x27, CPUInstruction("DAA", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t add = 0;
		uint8_t lowerHalfByte = inProcessor.A & 0x0F;
		if (inProcessor.Flags.AC || (lowerHalfByte > 9))
		{
			add += 6;
			inProcessor.Flags.AC = ((lowerHalfByte + 6) > 0x0F);
		}
		else
		{
			inProcessor.Flags.AC = false;
		}
		uint8_t higherHalfByte = (inProcessor.A >> 4);
		if (inProcessor.Flags.C || (higherHalfByte > 9) || (inProcessor.Flags.AC && (higherHalfByte == 9)))
		{
			add += 0x60;
			inProcessor.Flags.C = inProcessor.Flags.C || (static_cast<uint16_t>(inProcessor.A + add) > 0xFF);
		}
		inProcessor.A = inProcessor.A + add;
		inProcessor.updateFlagsForAccumulator();
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x29, CPUInstruction("DAD H", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addRegisterPairToHL(inProcessor, inProcessor.getHLPairValue());
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x2A, MemoryCPUInstruction("LHLD", 16,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t highByte = 0;
		uint8_t lowByte = 0;
		if (!inInstruction.getArgumentValue(1, highByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inInstruction.getArgumentValue(0, lowByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		const uint16_t memoryAddress = combineTwoBytesToWord(highByte, lowByte);
		if (!inProcessor.getByteInMemory(memoryAddress, inProcessor.L))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.getByteInMemory(memoryAddress + 1, inProcessor.H))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x2B, CPUInstruction("DCX H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegisterPairValue(inProcessor.H, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x2C, CPUInstruction("INR L", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegister(inProcessor, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x2D, CPUInstruction("DCR L", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegister(inProcessor, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x2E, CPUInstruction("MVI L,", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return moveImmediateValueToByte(inProcessor.L, inInstruction);
	}, 1));
	CPUInstructions.emplace(0x2F, CPUInstruction("CMA", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.A = ~inProcessor.A;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x31, CPUInstruction("LXI SP,", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t highByte = 0;
		uint8_t lowByte = 0;
		if (!inInstruction.getArgumentValue(1, highByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inInstruction.getArgumentValue(0, lowByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		inProcessor.SP = combineTwoBytesToWord(highByte, lowByte);
		return EInstructionExecutionResult::Success;
	}, 2));
	CPUInstructions.emplace(0x32, MemoryCPUInstruction("STA", 13,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t highByte = 0;
		uint8_t lowByte = 0;
		if (!inInstruction.getArgumentValue(1, highByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inInstruction.getArgumentValue(0, lowByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.setByteInMemory(highByte, lowByte, inProcessor.A))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x33, CPUInstruction("INX SP", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		++inProcessor.SP;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x34, CPUInstruction("INR M", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		inProcessor.Flags.AC = ((byteInMemory & 0x0F) + 1) > 0x0F;
		++byteInMemory;
		if (!inProcessor.setByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		inProcessor.updateFlagsForByte(byteInMemory);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x35, CPUInstruction("DCR M", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		inProcessor.Flags.AC = ((byteInMemory & 0x0F) + 0x0F) > 0x0F;	// Add (~0x1 + 1) and check if we have carry on 4th bit
		--byteInMemory;
		if (!inProcessor.setByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		inProcessor.updateFlagsForByte(byteInMemory);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x36, CPUInstruction("MVI M,", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t argumentValue = 0;
		if (!inInstruction.getArgumentValue(0, argumentValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.setByteAtAddressStoredInHL(argumentValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0x37, CPUInstruction("STC", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.Flags.C = true;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x39, CPUInstruction("DAD SP", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addRegisterPairToHL(inProcessor, inProcessor.SP);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x3A, MemoryCPUInstruction("LDA", 13,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t highByte = 0;
		uint8_t lowByte = 0;
		if (!inInstruction.getArgumentValue(1, highByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inInstruction.getArgumentValue(0, lowByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.getByteInMemory(highByte, lowByte, inProcessor.A))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x3B, CPUInstruction("DCX SP", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		--inProcessor.SP;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x3C, CPUInstruction("INR A", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		incrementRegister(inProcessor, inProcessor.A);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x3D, CPUInstruction("DCR A", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		decrementRegister(inProcessor, inProcessor.A);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x3E, CPUInstruction("MVI A,", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return moveImmediateValueToByte(inProcessor.A, inInstruction);
	}, 1));
	CPUInstructions.emplace(0x3F, CPUInstruction("CMC", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.Flags.C = !inProcessor.Flags.C;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x40, CPUInstruction("MOV B, B", 5,
	[](Processor&, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x41, CPUInstruction("MOV B, C", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.B = inProcessor.C;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x42, CPUInstruction("MOV B, D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.B = inProcessor.D;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x43, CPUInstruction("MOV B, E", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.B = inProcessor.E;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x44, CPUInstruction("MOV B, H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.B = inProcessor.H;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x45, CPUInstruction("MOV B, L", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.B = inProcessor.L;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x46, CPUInstruction("MOV B, M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.getByteAtAddressStoredInHL(inProcessor.B) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x47, CPUInstruction("MOV B, A", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.B = inProcessor.A;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x48, CPUInstruction("MOV C, B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.C = inProcessor.B;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x49, CPUInstruction("MOV C, C", 5,
	[](Processor&, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x4A, CPUInstruction("MOV C, D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.C = inProcessor.D;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x4B, CPUInstruction("MOV C, E", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.C = inProcessor.E;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x4C, CPUInstruction("MOV C, H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.C = inProcessor.H;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x4D, CPUInstruction("MOV C, L", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.C = inProcessor.L;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x4E, CPUInstruction("MOV C, M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.getByteAtAddressStoredInHL(inProcessor.C) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x4F, CPUInstruction("MOV C, A", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.C = inProcessor.A;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x50, CPUInstruction("MOV D, B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.D = inProcessor.B;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x51, CPUInstruction("MOV D, C", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.D = inProcessor.C;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x52, CPUInstruction("MOV D, D", 5,
	[](Processor&, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x53, CPUInstruction("MOV D, E", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.D = inProcessor.E;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x54, CPUInstruction("MOV D, H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.D = inProcessor.H;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x55, CPUInstruction("MOV D, L", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.D = inProcessor.L;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x56, CPUInstruction("MOV D, M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.getByteAtAddressStoredInHL(inProcessor.D) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x57, CPUInstruction("MOV D, A", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.D = inProcessor.A;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x58, CPUInstruction("MOV E, B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.E = inProcessor.B;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x59, CPUInstruction("MOV E, C", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.E = inProcessor.C;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x5A, CPUInstruction("MOV E, D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.E = inProcessor.D;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x5B, CPUInstruction("MOV E, E", 5,
	[](Processor&, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x5C, CPUInstruction("MOV E, H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.E = inProcessor.H;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x5D, CPUInstruction("MOV E, L", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.E = inProcessor.L;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x5E, CPUInstruction("MOV E, M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.getByteAtAddressStoredInHL(inProcessor.E) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x5F, CPUInstruction("MOV E, A", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.E = inProcessor.A;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x60, CPUInstruction("MOV H, B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.H = inProcessor.B;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x61, CPUInstruction("MOV H, C", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.H = inProcessor.C;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x62, CPUInstruction("MOV H, D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.H = inProcessor.D;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x63, CPUInstruction("MOV H, E", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.H = inProcessor.E;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x64, CPUInstruction("MOV H, H", 5,
	[](Processor&, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x65, CPUInstruction("MOV H, L", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.H = inProcessor.L;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x66, CPUInstruction("MOV H, M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.getByteAtAddressStoredInHL(inProcessor.H) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x67, CPUInstruction("MOV H, A", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.H = inProcessor.A;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x68, CPUInstruction("MOV L, B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.L = inProcessor.B;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x69, CPUInstruction("MOV L, C", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.L = inProcessor.C;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x6A, CPUInstruction("MOV L, D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.L = inProcessor.D;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x6B, CPUInstruction("MOV L, E", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.L = inProcessor.E;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x6C, CPUInstruction("MOV L, H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.L = inProcessor.H;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x6D, CPUInstruction("MOV L, L", 5,
	[](Processor&, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x6E, CPUInstruction("MOV L, M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.getByteAtAddressStoredInHL(inProcessor.L) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x6F, CPUInstruction("MOV L, A", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.L = inProcessor.A;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x70, CPUInstruction("MOV M, B", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.setByteAtAddressStoredInHL(inProcessor.B) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x71, CPUInstruction("MOV M, C", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.setByteAtAddressStoredInHL(inProcessor.C) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x72, CPUInstruction("MOV M, D", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.setByteAtAddressStoredInHL(inProcessor.D) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x73, CPUInstruction("MOV M, E", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.setByteAtAddressStoredInHL(inProcessor.E) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x74, CPUInstruction("MOV M, H", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.setByteAtAddressStoredInHL(inProcessor.H) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x75, CPUInstruction("MOV M, L", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.setByteAtAddressStoredInHL(inProcessor.L) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x76, CPUInstruction("HLT", 7,
	[](Processor& inProcessor, const CPUInstruction&)
	{
		inProcessor.setHalted(true);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x77, CPUInstruction("MOV M, A", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.setByteAtAddressStoredInHL(inProcessor.A) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x78, CPUInstruction("MOV A, B", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.A = inProcessor.B;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x79, CPUInstruction("MOV A, C", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.A = inProcessor.C;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x7A, CPUInstruction("MOV A, D", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.A = inProcessor.D;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x7B, CPUInstruction("MOV A, E", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.A = inProcessor.E;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x7C, CPUInstruction("MOV A, H", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.A = inProcessor.H;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x7D, CPUInstruction("MOV A, L", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.A = inProcessor.L;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x7E, CPUInstruction("MOV A, M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return inProcessor.getByteAtAddressStoredInHL(inProcessor.A) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError;
	}));
	CPUInstructions.emplace(0x7F, CPUInstruction("MOV A, A", 5,
	[](Processor&, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x80, CPUInstruction("ADD B", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.B);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x81, CPUInstruction("ADD C", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x82, CPUInstruction("ADD D", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.D);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x83, CPUInstruction("ADD E", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x84, CPUInstruction("ADD H", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.H);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x85, CPUInstruction("ADD L", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x86, CPUInstruction("ADD M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		addToAccumulator(inProcessor, byteInMemory);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x87, CPUInstruction("ADD A", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.A);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x88, CPUInstruction("ADC B", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.B + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x89, CPUInstruction("ADC C", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.C + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x8A, CPUInstruction("ADC D", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.D + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x8B, CPUInstruction("ADC E", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.E + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x8C, CPUInstruction("ADC H", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.H + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x8D, CPUInstruction("ADC L", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.L + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x8E, CPUInstruction("ADC M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		addToAccumulator(inProcessor, byteInMemory + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x8F, CPUInstruction("ADC A", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		addToAccumulator(inProcessor, inProcessor.A + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x90, CPUInstruction("SUB B", 4,
	[](Processor& inProcessor, const CPUInstruction&)
	{
		subtractFromAccumulator(inProcessor, inProcessor.B);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x91, CPUInstruction("SUB C", 4,
	[](Processor& inProcessor, const CPUInstruction&)
	{
		subtractFromAccumulator(inProcessor, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x92, CPUInstruction("SUB D", 4,
	[](Processor& inProcessor, const CPUInstruction&)
	{
		subtractFromAccumulator(inProcessor, inProcessor.D);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x93, CPUInstruction("SUB E", 4,
	[](Processor& inProcessor, const CPUInstruction&)
	{
		subtractFromAccumulator(inProcessor, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x94, CPUInstruction("SUB H", 4,
	[](Processor& inProcessor, const CPUInstruction&)
	{
		subtractFromAccumulator(inProcessor, inProcessor.H);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x95, CPUInstruction("SUB L", 4,
	[](Processor& inProcessor, const CPUInstruction&)
	{
		subtractFromAccumulator(inProcessor, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x96, CPUInstruction("SUB M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		subtractFromAccumulator(inProcessor, byteInMemory);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x97, CPUInstruction("SUB A", 4,
	[](Processor& inProcessor, const CPUInstruction&)
	{
		subtractFromAccumulator(inProcessor, inProcessor.A);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x98, CPUInstruction("SBB B", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		subtractFromAccumulator(inProcessor, inProcessor.B + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x99, CPUInstruction("SBB C", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		subtractFromAccumulator(inProcessor, inProcessor.C + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x9A, CPUInstruction("SBB D", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		subtractFromAccumulator(inProcessor, inProcessor.D + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x9B, CPUInstruction("SBB E", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		subtractFromAccumulator(inProcessor, inProcessor.E + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x9C, CPUInstruction("SBB H", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		subtractFromAccumulator(inProcessor, inProcessor.H + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x9D, CPUInstruction("SBB L", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		subtractFromAccumulator(inProcessor, inProcessor.L + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x9E, CPUInstruction("SBB M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		subtractFromAccumulator(inProcessor, byteInMemory + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0x9F, CPUInstruction("SBB A", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		subtractFromAccumulator(inProcessor, inProcessor.A + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA0, CPUInstruction("ANA B", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performANDOperationWithAccumulator(inProcessor, inProcessor.B);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA1, CPUInstruction("ANA C", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performANDOperationWithAccumulator(inProcessor, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA2, CPUInstruction("ANA D", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performANDOperationWithAccumulator(inProcessor, inProcessor.D);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA3, CPUInstruction("ANA E", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performANDOperationWithAccumulator(inProcessor, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA4, CPUInstruction("ANA H", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performANDOperationWithAccumulator(inProcessor, inProcessor.H);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA5, CPUInstruction("ANA L", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performANDOperationWithAccumulator(inProcessor, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA6, CPUInstruction("ANA M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		performANDOperationWithAccumulator(inProcessor, byteInMemory);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA7, CPUInstruction("ANA A", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performANDOperationWithAccumulator(inProcessor, inProcessor.A);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA8, CPUInstruction("XRA B", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performXOROperationWithAccumulator(inProcessor, inProcessor.B);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xA9, CPUInstruction("XRA C", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performXOROperationWithAccumulator(inProcessor, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xAA, CPUInstruction("XRA D", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performXOROperationWithAccumulator(inProcessor, inProcessor.D);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xAB, CPUInstruction("XRA E", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performXOROperationWithAccumulator(inProcessor, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xAC, CPUInstruction("XRA H", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performXOROperationWithAccumulator(inProcessor, inProcessor.H);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xAD, CPUInstruction("XRA L", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performXOROperationWithAccumulator(inProcessor, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xAE, CPUInstruction("XRA M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		performXOROperationWithAccumulator(inProcessor, byteInMemory);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xAF, CPUInstruction("XRA A", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performXOROperationWithAccumulator(inProcessor, inProcessor.A);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB0, CPUInstruction("ORA B", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performOROperationWithAccumulator(inProcessor, inProcessor.B);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB1, CPUInstruction("ORA C", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performOROperationWithAccumulator(inProcessor, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB2, CPUInstruction("ORA D", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performOROperationWithAccumulator(inProcessor, inProcessor.D);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB3, CPUInstruction("ORA E", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performOROperationWithAccumulator(inProcessor, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB4, CPUInstruction("ORA H", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performOROperationWithAccumulator(inProcessor, inProcessor.H);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB5, CPUInstruction("ORA L", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performOROperationWithAccumulator(inProcessor, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB6, CPUInstruction("ORA M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		performOROperationWithAccumulator(inProcessor, byteInMemory);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB7, CPUInstruction("ORA A", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		performOROperationWithAccumulator(inProcessor, inProcessor.A);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB8, CPUInstruction("CMP B", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		compareWithAccumulator(inProcessor, inProcessor.B);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xB9, CPUInstruction("CMP C", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		compareWithAccumulator(inProcessor, inProcessor.C);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xBA, CPUInstruction("CMP D", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		compareWithAccumulator(inProcessor, inProcessor.D);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xBB, CPUInstruction("CMP E", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		compareWithAccumulator(inProcessor, inProcessor.E);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xBC, CPUInstruction("CMP H", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		compareWithAccumulator(inProcessor, inProcessor.H);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xBD, CPUInstruction("CMP L", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		compareWithAccumulator(inProcessor, inProcessor.L);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xBE, CPUInstruction("CMP M", 7,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inProcessor.getByteAtAddressStoredInHL(byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		compareWithAccumulator(inProcessor, byteInMemory);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xBF, CPUInstruction("CMP A", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.updateFlagsForByte(0);
		inProcessor.Flags.C = false;
		inProcessor.Flags.AC = true;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xC0, CPUInstruction("RNZ", 5,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return retOnCondition(inProcessor, inInstruction, !inProcessor.Flags.Z);
	}, 0, 11));
	CPUInstructions.emplace(0xC1, CPUInstruction("POP B", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return (inProcessor.popFromStackToBytes(inProcessor.B, inProcessor.C) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError);
	}));
	CPUInstructions.emplace(0xC2, MemoryCPUInstruction("JNZ", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return jumpOnCondition(inProcessor, inInstruction, !inProcessor.Flags.Z);
	}));
	CPUInstructions.emplace(0xC3, jmpInstruction);
	CPUInstructions.emplace(0xC4, MemoryCPUInstruction("CNZ", 11,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return callOnCondition(inProcessor, inInstruction, !inProcessor.Flags.Z);
	}, 17));
	CPUInstructions.emplace(0xC5, CPUInstruction("PUSH B", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return (inProcessor.pushBytesPairOnStack(inProcessor.B, inProcessor.C) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError);
	}));
	CPUInstructions.emplace(0xC6, CPUInstruction("ADI", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t immediateValue = 0;
		if (!inInstruction.getArgumentValue(0, immediateValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		addToAccumulator(inProcessor, immediateValue);
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xC7, CPUInstruction("RST 0", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return performReset(inProcessor, 0);
	}));
	CPUInstructions.emplace(0xC8, CPUInstruction("RZ", 5,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return retOnCondition(inProcessor, inInstruction, inProcessor.Flags.Z);
	}, 0, 11));
	CPUInstructions.emplace(0xC9, retInstruction);
	CPUInstructions.emplace(0xCA, MemoryCPUInstruction("JZ", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return jumpOnCondition(inProcessor, inInstruction, inProcessor.Flags.Z);
	}));
	CPUInstructions.emplace(0xCC, MemoryCPUInstruction("CZ", 11,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return callOnCondition(inProcessor, inInstruction, inProcessor.Flags.Z);
	}, 17));
	CPUInstructions.emplace(0xCD, callInstruction);
	CPUInstructions.emplace(0xCE, CPUInstruction("ACI", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t immediateValue = 0;
		if (!inInstruction.getArgumentValue(0, immediateValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		addToAccumulator(inProcessor, immediateValue + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xCF, CPUInstruction("RST 1", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return performReset(inProcessor, 1);
	}));
	CPUInstructions.emplace(0xD0, CPUInstruction("RNC", 5,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return retOnCondition(inProcessor, inInstruction, !inProcessor.Flags.C);
	}, 0, 11));
	CPUInstructions.emplace(0xD1, CPUInstruction("POP D", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return (inProcessor.popFromStackToBytes(inProcessor.D, inProcessor.E) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError);
	}));
	CPUInstructions.emplace(0xD2, MemoryCPUInstruction("JNC", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return jumpOnCondition(inProcessor, inInstruction, !inProcessor.Flags.C);
	}));
	CPUInstructions.emplace(0xD3, CPUInstruction("OUT", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t connectionPort = 0;
		if (!inInstruction.getArgumentValue(0, connectionPort))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.hasOutputDeviceConnectedToPort(connectionPort))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		const OutputDeviceConnection& deviceConnection = inProcessor.getOutputDeviceConnectionAtPort(connectionPort);
		if (!deviceConnection.passOutputByte(inProcessor.A))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xD4, MemoryCPUInstruction("CNC", 11,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return callOnCondition(inProcessor, inInstruction, !inProcessor.Flags.C);
	}, 17));
	CPUInstructions.emplace(0xD5, CPUInstruction("PUSH D", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return (inProcessor.pushBytesPairOnStack(inProcessor.D, inProcessor.E) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError);
	}));
	CPUInstructions.emplace(0xD6, CPUInstruction("SUI", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t immediateValue = 0;
		if (!inInstruction.getArgumentValue(0, immediateValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		subtractFromAccumulator(inProcessor, immediateValue);
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xD7, CPUInstruction("RST 2", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return performReset(inProcessor, 2);
	}));
	CPUInstructions.emplace(0xD8, CPUInstruction("RC", 5,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return retOnCondition(inProcessor, inInstruction, inProcessor.Flags.C);
	}, 0, 11));
	CPUInstructions.emplace(0xDA, MemoryCPUInstruction("JC", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return jumpOnCondition(inProcessor, inInstruction, inProcessor.Flags.C);
	}));
	CPUInstructions.emplace(0xDB, CPUInstruction("IN", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t connectionPort = 0;
		if (!inInstruction.getArgumentValue(0, connectionPort))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.hasInputDeviceConnectedToPort(connectionPort))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		const InputDeviceConnection& deviceConnection = inProcessor.getInputDeviceConnectionAtPort(connectionPort);
		if (!deviceConnection.getInputByte(inProcessor.A))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xDC, MemoryCPUInstruction("CC", 11,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return callOnCondition(inProcessor, inInstruction, inProcessor.Flags.C);
	}, 17));
	CPUInstructions.emplace(0xDE, CPUInstruction("SBI", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t argumentValue = 0;
		if (!inInstruction.getArgumentValue(0, argumentValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		subtractFromAccumulator(inProcessor, argumentValue + (inProcessor.Flags.C ? 1 : 0));
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xDF, CPUInstruction("RST 3", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return performReset(inProcessor, 3);
	}));
	CPUInstructions.emplace(0xE0, CPUInstruction("RPO", 5,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return retOnCondition(inProcessor, inInstruction, !inProcessor.Flags.P);
	}, 0, 11));
	CPUInstructions.emplace(0xE1, CPUInstruction("POP H", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return (inProcessor.popFromStackToBytes(inProcessor.H, inProcessor.L) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError);
	}));
	CPUInstructions.emplace(0xE2, MemoryCPUInstruction("JPO", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return jumpOnCondition(inProcessor, inInstruction, !inProcessor.Flags.P);
	}));
	CPUInstructions.emplace(0xE3, CPUInstruction("XTHL", 18,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t currentH = inProcessor.H;
		uint8_t currentL = inProcessor.L;
		if (!inProcessor.getBytesPairStoredAtSP(inProcessor.H, inProcessor.L))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.setByteInMemory(inProcessor.SP, currentL))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		if (!inProcessor.setByteInMemory(inProcessor.SP + 1, currentH))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xE4, MemoryCPUInstruction("CPO", 11,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return callOnCondition(inProcessor, inInstruction, !inProcessor.Flags.P);
	}, 17));
	CPUInstructions.emplace(0xE5, CPUInstruction("PUSH H", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return (inProcessor.pushBytesPairOnStack(inProcessor.H, inProcessor.L) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError);
	}));
	CPUInstructions.emplace(0xE6, CPUInstruction("ANI", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t argumentValue = 0;
		if (!inInstruction.getArgumentValue(0, argumentValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		performANDOperationWithAccumulator(inProcessor, argumentValue);
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xE7, CPUInstruction("RST 4", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return performReset(inProcessor, 4);
	}));
	CPUInstructions.emplace(0xE8, CPUInstruction("RPE", 5,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return retOnCondition(inProcessor, inInstruction, inProcessor.Flags.P);
	}, 0, 11));
	CPUInstructions.emplace(0xE9, CPUInstruction("PCHL", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.setPC(inProcessor.H, inProcessor.L);
		return EInstructionExecutionResult::KeepProgramCounter;
	}));
	CPUInstructions.emplace(0xEA, MemoryCPUInstruction("JPE", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return jumpOnCondition(inProcessor, inInstruction, inProcessor.Flags.P);
	}));
	CPUInstructions.emplace(0xEB, CPUInstruction("XCHG", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		const uint8_t currentH = inProcessor.H;
		const uint8_t currentL = inProcessor.L;
		inProcessor.H = inProcessor.D;
		inProcessor.L = inProcessor.E;
		inProcessor.D = currentH;
		inProcessor.E = currentL;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xEC, MemoryCPUInstruction("CPE", 11,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return callOnCondition(inProcessor, inInstruction, inProcessor.Flags.P);
	}, 17));
	CPUInstructions.emplace(0xEE, CPUInstruction("XRI", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t argumentValue = 0;
		if (!inInstruction.getArgumentValue(0, argumentValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		performXOROperationWithAccumulator(inProcessor, argumentValue);
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xEF, CPUInstruction("RST 5", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return performReset(inProcessor, 5);
	}));
	CPUInstructions.emplace(0xF0, CPUInstruction("RP", 5,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return retOnCondition(inProcessor, inInstruction, !inProcessor.Flags.S);
	}, 0, 11));
	CPUInstructions.emplace(0xF1, CPUInstruction("POP PSW", 10,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t stackHighByte = 0;
		uint8_t stackLowByte = 0;
		if (!inProcessor.popFromStackToBytes(stackHighByte, stackLowByte))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		inProcessor.A = stackHighByte;
		inProcessor.Flags.C = (stackLowByte & 0x01) != 0;
		inProcessor.Flags.P = (stackLowByte & 0x04) != 0;
		inProcessor.Flags.AC = (stackLowByte & 0x10) != 0;
		inProcessor.Flags.Z = (stackLowByte & 0x40) != 0;
		inProcessor.Flags.S = (stackLowByte & 0x80) != 0;
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xF2, MemoryCPUInstruction("JP", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return jumpOnCondition(inProcessor, inInstruction, !inProcessor.Flags.S);
	}));
	CPUInstructions.emplace(0xF3, CPUInstruction("DI", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.setInterruptsEnabled(false);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xF4, MemoryCPUInstruction("CP", 11,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return callOnCondition(inProcessor, inInstruction, !inProcessor.Flags.S);
	}, 17));
	CPUInstructions.emplace(0xF5, CPUInstruction("PUSH PSW", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		uint8_t flagsRegister = 0x02;
		flagsRegister = flagsRegister | (inProcessor.Flags.C ? 0x01 : 0x00);
		flagsRegister = flagsRegister | (inProcessor.Flags.P ? 0x04 : 0x00);
		flagsRegister = flagsRegister | (inProcessor.Flags.AC ? 0x10 : 0x00);
		flagsRegister = flagsRegister | (inProcessor.Flags.Z ? 0x40 : 0x00);
		flagsRegister = flagsRegister | (inProcessor.Flags.S ? 0x80 : 0x00);
		return (inProcessor.pushBytesPairOnStack(inProcessor.A, flagsRegister) ? EInstructionExecutionResult::Success : EInstructionExecutionResult::ExecutionError);
	}));
	CPUInstructions.emplace(0xF6, CPUInstruction("ORI", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t byteInMemory = 0;
		if (!inInstruction.getArgumentValue(0, byteInMemory))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		performOROperationWithAccumulator(inProcessor, byteInMemory);
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xF7, CPUInstruction("RST 6", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return performReset(inProcessor, 6);
	}));
	CPUInstructions.emplace(0xF8, CPUInstruction("RM", 5,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return retOnCondition(inProcessor, inInstruction, inProcessor.Flags.S);
	}, 0, 11));
	CPUInstructions.emplace(0xF9, CPUInstruction("SPHL", 5,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.SP = inProcessor.getHLPairValue();
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xFA, MemoryCPUInstruction("JM", 10,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return jumpOnCondition(inProcessor, inInstruction, inProcessor.Flags.S);
	}));
	CPUInstructions.emplace(0xFB, CPUInstruction("EI", 4,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		inProcessor.setInterruptsEnabled(true);
		return EInstructionExecutionResult::Success;
	}));
	CPUInstructions.emplace(0xFC, MemoryCPUInstruction("CM", 11,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		return callOnCondition(inProcessor, inInstruction, inProcessor.Flags.S);
	}, 17));
	CPUInstructions.emplace(0xFE, CPUInstruction("CPI", 7,
	[](Processor& inProcessor, const CPUInstruction& inInstruction) -> EInstructionExecutionResult
	{
		uint8_t argumentValue = 0;
		if (!inInstruction.getArgumentValue(0, argumentValue))
		{
			return EInstructionExecutionResult::ExecutionError;
		}
		compareWithAccumulator(inProcessor, argumentValue);
		return EInstructionExecutionResult::Success;
	}, 1));
	CPUInstructions.emplace(0xFF, CPUInstruction("RST 7", 11,
	[](Processor& inProcessor, const CPUInstruction&) -> EInstructionExecutionResult
	{
		return performReset(inProcessor, 7);
	}));
	
	CPUInstructions.emplace(0x08, nopInstruction);
	CPUInstructions.emplace(0x10, nopInstruction);
	CPUInstructions.emplace(0x18, nopInstruction);
	CPUInstructions.emplace(0x20, nopInstruction);
	CPUInstructions.emplace(0x28, nopInstruction);
	CPUInstructions.emplace(0x30, nopInstruction);
	CPUInstructions.emplace(0x38, nopInstruction);
	CPUInstructions.emplace(0xCB, jmpInstruction);
	CPUInstructions.emplace(0xD9, retInstruction);
	CPUInstructions.emplace(0xDD, callInstruction);
	CPUInstructions.emplace(0xED, callInstruction);
	CPUInstructions.emplace(0xFD, callInstruction);

	return CPUInstructions;
}
