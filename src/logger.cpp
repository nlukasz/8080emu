#include <sstream>
#include "logger.hpp"

const std::string Logger::logFilePath = "8080emulator.txt";
Logger Logger::staticLogger;

void Logger::logMessage(const std::string inMessage, const bool inAddNewLine)
{
	staticLogger.logMessageInternal(inMessage, inAddNewLine);
}

void Logger::logErrorMessage(const std::string inMessage, const bool inAddNewLine)
{
	staticLogger.logMessageInternal("Error: " + inMessage, inAddNewLine);
}

void Logger::logNewLine()
{
	staticLogger.logMessageInternal("", true);
}

Logger::~Logger()
{
	if (canLogToFile())
	{
		if (MAXIMUM_LOG_FILE_LINES_COUNT > 0)
		{
			for (unsigned int i = 0; i < fileLines.size(); ++i)
			{
				loggerFileStream << fileLines[i];
			}
			fileLines.clear();
		}
		
		loggerFileStream.close();
	}
}

Logger::Logger()
{
	if (printToFile)
	{
		loggerFileStream.open(logFilePath);
		if (MAXIMUM_LOG_FILE_LINES_COUNT > 0)
		{
			fileLines.reserve(MAXIMUM_LOG_FILE_LINES_COUNT);
		}
	}
}

bool Logger::canLogToFile() const
{
	return (printToFile && loggerFileStream.is_open());
}

void Logger::logMessageInternal(const std::string& inMessage, const bool& inAddNewLine)
{
	std::stringstream messageStream;
	messageStream << inMessage;
	if (inAddNewLine)
	{
		messageStream << std::endl;
	}
	
	if (printToStandardOutput)
	{
		std::cout << messageStream.str();
	}
	if (canLogToFile())
	{
		if (MAXIMUM_LOG_FILE_LINES_COUNT > 0)
		{
			if (fileLines.size() >= MAXIMUM_LOG_FILE_LINES_COUNT)
			{
				fileLines.erase(fileLines.begin());
			}
			fileLines.push_back(messageStream.str());
		}
		else
		{
			loggerFileStream << messageStream.str();
		}
	}
}
