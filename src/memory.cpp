#include "memory.hpp"

Memory::Memory(const unsigned int inMemorySize) : memorySize(inMemorySize)
{
	if (inMemorySize > 0)
	{
		memory = new uint8_t[inMemorySize];
	}
}

Memory::~Memory()
{
	if (memory != nullptr)
	{
		delete[] memory;
		memory = nullptr;
	}
}

// Getters
unsigned int Memory::getMemorySize() const
{
	return memorySize;
}

bool Memory::getByteAtAddress(const uint16_t inAddress, uint8_t& outByte) const
{
	if (!validateMemoryAddress(inAddress))
	{
		return false;
	}
	outByte = memory[inAddress];
	return true;
}

bool Memory::getBytePairAtAddress(const uint16_t inAddress, uint8_t& outHighByte, uint8_t& outLowByte) const
{
	if (!validateMemoryAddress(inAddress + 1))
	{
		return false;
	}
	outLowByte = memory[inAddress];
	outHighByte = memory[inAddress + 1];
	return true;
}

// Setters
bool Memory::setByteAtAddress(const uint16_t inAddress, const uint8_t inByte)
{
	if (!validateMemoryAddress(inAddress))
	{
		return false;
	}
	memory[inAddress] = inByte;
	return true;
}

bool Memory::setBytesPairAtAddress(const uint16_t inAddress, const uint8_t inHighByte, const uint8_t inLowByte)
{
	if (!validateMemoryAddress(inAddress) || (inAddress < 1))
	{
		return false;
	}
	memory[inAddress] = inHighByte;
	memory[inAddress - 1] = inLowByte;
	return true;
}

// Helper functions
bool Memory::validateMemoryAddress(const uint16_t inMemoryIndex) const
{
	if (!isValid())
	{
		return false;
	}
	if (memorySize <= MAX_MEMORY_ADDRESS)
	{
		if (inMemoryIndex >= memorySize)
		{
			return false;
		}
	}
	return true;
}

inline bool Memory::isValid() const
{
	return (memory != nullptr);
}
