#include "arguments_handler.hpp"
#include <unordered_map>
#include <set>

std::unordered_map<EArgumentOptionType, std::string> getAvailableArguments()
{
	std::unordered_map<EArgumentOptionType, std::string> availableArguments;
	availableArguments.emplace(EArgumentOptionType::DisassemblyROM, "-d");
	availableArguments.emplace(EArgumentOptionType::SetBufferSize, "-b");
	availableArguments.emplace(EArgumentOptionType::ROMStartAddress, "-org");
	return availableArguments;
}

std::pair<EArgumentParseResult, std::string> parseProgramArguments(const int argc, const char* const* argv, std::vector<ProgramArgumentData>& outArgumentsData, std::string& outROMPath)
{
	std::unordered_map<EArgumentOptionType, std::string> availableArguments = getAvailableArguments();
	std::set<EArgumentOptionType> usedArguments;
	std::string ROMFilePath = "";
	std::pair<EArgumentParseResult, std::string> parseResult(EArgumentParseResult::Success, "");
	
	for (int i = 1; i < argc; ++i)
	{
		std::string currentArgument = argv[i];
		EArgumentOptionType currentArgumentType = EArgumentOptionType::None;
		for (const auto& argumentPair : availableArguments)
		{
			if (currentArgument.compare(argumentPair.second) == 0)
			{
				currentArgumentType = argumentPair.first;
				break;
			}
		}
		
		if (usedArguments.find(currentArgumentType) != usedArguments.end())
		{
			parseResult.first = EArgumentParseResult::ArgumentAlreadyUsed;
			parseResult.second = currentArgument;
			return parseResult;
		}
		
		ProgramArgumentData newArgumentData(currentArgumentType);
		switch(currentArgumentType)
		{
			case EArgumentOptionType::SetBufferSize:
			{
				++i;
				if (i >= argc)
				{
					parseResult.first = EArgumentParseResult::NoBufferSizeSpecified;
					break;
				}
				int bufferSize = -1;
				try
				{
					bufferSize = std::stoi(argv[i]);
				}
				catch (...) {}
				if (bufferSize <= 0)
				{
					parseResult.first = EArgumentParseResult::InvalidBufferSize;
					break;
				}
				newArgumentData.integerValue = bufferSize;
				break;
			}
			case EArgumentOptionType::ROMStartAddress:
			{
				++i;
				if (i >= argc)
				{
					parseResult.first = EArgumentParseResult::NoBufferSizeSpecified;
					break;
				}
				int startAddress = -1;
				try
				{
					startAddress = std::stoi(argv[i]);
				}
				catch (...) {}
				if ((startAddress < 0) || (startAddress >= 65536))
				{
					parseResult.first = EArgumentParseResult::InvalidROMStartAddress;
					break;
				}
				newArgumentData.integerValue = startAddress;
				break;
			}
			case EArgumentOptionType::None:
			{
				if (!ROMFilePath.empty())
				{
					parseResult.first = EArgumentParseResult::InvalidArgument;
					break;
				}
				ROMFilePath = argv[i];
				break;
			}
			default:
				break;
		}
		
		if (parseResult.first != EArgumentParseResult::Success)
		{
			parseResult.second = currentArgument;
			return parseResult;
		}
		
		if (currentArgumentType != EArgumentOptionType::None)
		{
			usedArguments.emplace(currentArgumentType);
			outArgumentsData.push_back(newArgumentData);
		}
	}
	
	if (ROMFilePath.empty())
	{
		parseResult.first = EArgumentParseResult::NoROMFilePath;
		return parseResult;
	}
	outROMPath = ROMFilePath;
	
	return parseResult;
}

std::string getArgumentParseErrorMessage(const std::pair<EArgumentParseResult, std::string>& argumentsParseResult)
{
	switch(argumentsParseResult.first)
	{
		case EArgumentParseResult::NoROMFilePath:
			return "ROM file path missing!";
		case EArgumentParseResult::InvalidArgument:
			return std::string("Unrecognized argument used: ") + argumentsParseResult.second;
		case EArgumentParseResult::ArgumentAlreadyUsed:
			return std::string("Argument used more than one time: ") + argumentsParseResult.second;
		case EArgumentParseResult::NoBufferSizeSpecified:
			return "No read buffer size was specified!";
		case EArgumentParseResult::InvalidBufferSize:
			return "Given buffer size is invalid!";
		case EArgumentParseResult::NoROMStartAddressSpecified:
			return "ROM start address was not specified!";
		case EArgumentParseResult::InvalidROMStartAddress:
			return "Given ROM start address is invalid!";
		default:
			return "Unknown error!";
	}
}
