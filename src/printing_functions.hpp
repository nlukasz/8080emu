#ifndef printing_functions_hpp
#define printing_functions_hpp

#include "instruction_data.hpp"
#include <string>

std::string getNumberInHexString(const unsigned int inNumberToPrint, const unsigned int inWidth = 2);

std::string getInstructionString(const CPUInstruction& inInstruction, const bool inPrintEndLine = true);

#endif /* printing_functions_hpp */
