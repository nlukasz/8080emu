#include "rom_file_reader.hpp"

RomFileReader::~RomFileReader()
{
	if (mROMFileStream.is_open())
	{
		mROMFileStream.close();
	}
	if (mReadBuffer != nullptr)
	{
		delete[] mReadBuffer;
		mReadBuffer = nullptr;
	}
}

bool RomFileReader::openROMFile()
{
	if (mROMFileStream.is_open())
	{
		return true;
	}
	
	mROMFileStream.open(mROMFilePath);
	if (!mROMFileStream.is_open())
	{
		return false;
	}
	
	mROMFileStream.seekg(0, mROMFileStream.end);
	mFileSize = static_cast<unsigned int>(mROMFileStream.tellg());
	mROMFileStream.seekg(0, mROMFileStream.beg);
	if (mFileSize < mBufferSize)
	{
		mBufferSize = mFileSize;
	}
	return true;
}

bool RomFileReader::getNextByte(uint8_t& outByte)
{
	if (!getByteAt(mCurrentPositionInFile, outByte))
	{
		return false;
	}
	++mCurrentPositionInFile;
	return true;
}

bool RomFileReader::isEOF() const
{
	return (mCurrentPositionInFile >= mFileSize);
}

unsigned int RomFileReader::getCurrentlyOpenedROMFileSize() const
{
	return mFileSize;
}

bool RomFileReader::readBytesToBuffer(unsigned int inPosition)
{
	mCurrentPositionInFile = inPosition;
	mBufferRange.first = inPosition;
	unsigned int bytesToRead = mBufferSize;
	if ((mFileSize - inPosition) < mBufferSize)
	{
		bytesToRead = mFileSize - inPosition;
		mBufferRange.second = (mFileSize - 1);
	}
	mROMFileStream.seekg(inPosition, mROMFileStream.beg);
	mROMFileStream.read(mReadBuffer, bytesToRead);
	mBufferRange.second = mBufferRange.first + static_cast<unsigned int>(mROMFileStream.gcount());
	
	return (mBufferRange.first != mBufferRange.second);
}

bool RomFileReader::getByteAt(unsigned int inPosition, uint8_t& outByte)
{
	if ((mBufferSize == 0) || !mROMFileStream.is_open() || (inPosition >= mFileSize))
	{
		return false;
	}
	
	if (mReadBuffer ==  nullptr)
	{
		mReadBuffer = new char[mBufferSize];
		if (!readBytesToBuffer(inPosition))
		{
			return false;
		}
	}
	
	if ((inPosition < mBufferRange.first) || (inPosition >= mBufferRange.second))
	{
		if (!readBytesToBuffer(inPosition))
		{
			return false;
		}
	}
	
	int indexInBuffer = inPosition - mBufferRange.first;
	outByte = static_cast<uint8_t>(mReadBuffer[indexInBuffer]);
	return true;
}
