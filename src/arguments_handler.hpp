#ifndef arguments_handler_h
#define arguments_handler_h

#include <string>
#include <vector>

enum class EArgumentParseResult
{
	Success = 0,
	// General errors
	NoROMFilePath,
	InvalidArgument,
	ArgumentAlreadyUsed,
	// Buffer size errors
	NoBufferSizeSpecified,
	InvalidBufferSize,
	// Initial Program Counter value
	NoROMStartAddressSpecified,
	InvalidROMStartAddress
};

enum class EArgumentOptionType
{
	None,
	DisassemblyROM,
	SetBufferSize,
	ROMStartAddress
};

struct ProgramArgumentData
{
	EArgumentOptionType argumentType;
	int integerValue;
	std::string textValue;
	
	ProgramArgumentData(EArgumentOptionType inArgumentType) : argumentType(inArgumentType) {}
};

std::pair<EArgumentParseResult, std::string> parseProgramArguments(const int argc, const char* const* argv, std::vector<ProgramArgumentData>& outArgumentsData, std::string& outROMPath);

std::string getArgumentParseErrorMessage(const std::pair<EArgumentParseResult, std::string>& argumentsParseResult);

#endif /* arguments_handler_h */
