#ifndef dummy_external_device_hpp
#define dummy_external_device_hpp

#include "external_device.hpp"

class DummyExternalDevice : public ExternalDevice
{
public:
	DummyExternalDevice() : ExternalDevice(getDummyDeviceSettings(), Memory(0)) {}
	static ExternalDeviceSettings getDummyDeviceSettings();
	
protected:
	void doExternalDeviceInitialization() override;
};

#endif /* dummy_external_device_hpp */
