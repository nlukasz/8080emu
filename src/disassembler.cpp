#include "disassembler.hpp"
#include "printing_functions.hpp"
#include <sstream>
#include "logger.hpp"

std::pair<EDisassemblyResult, std::string> writeDisassembledData(std::string inROMFilePath, unsigned int inReadBufferSize, const uint16_t inProgramStartAddress)
{
	std::pair<EDisassemblyResult, std::string> disassemblyResult(EDisassemblyResult::Success, "");
	
	RomFileReader romFileReader(inROMFilePath, inReadBufferSize);
	if (!romFileReader.openROMFile())
	{
		disassemblyResult.first = EDisassemblyResult::CantOpenROMFile;
		return disassemblyResult;
	}
	
	std::unordered_map<uint8_t, CPUInstruction> instructionsData = getCPUInstructions();
	
	uint8_t currentByte = '\0';
	uint16_t byteNumber = inProgramStartAddress;
	while (romFileReader.getNextByte(currentByte))
	{
		auto foundInstruction = instructionsData.find(currentByte);
		if (foundInstruction == instructionsData.end())
		{
			std::stringstream unrecognizedInstructionStream;
			unrecognizedInstructionStream << getNumberInHexString(byteNumber, 4);
			unrecognizedInstructionStream << ":\tUnrecognized instruction: 0x";
			unrecognizedInstructionStream << getNumberInHexString(currentByte);
			Logger::logMessage(unrecognizedInstructionStream.str());
			++byteNumber;
			continue;
		}
		
		CPUInstruction cpuInstruction = foundInstruction->second;
		Logger::logMessage(getNumberInHexString(byteNumber, 4), false);
		++byteNumber;
		const unsigned int parametersBytesCount = cpuInstruction.getArgumentsBytesCount();
		if (parametersBytesCount > 0)
		{
			for (unsigned int i = 0; i < parametersBytesCount; ++i)
			{
				if (!romFileReader.getNextByte(currentByte))
				{
					disassemblyResult.first = (romFileReader.isEOF() ? EDisassemblyResult::UnexpectedEOF : EDisassemblyResult::FileReadError);
					return disassemblyResult;
				}
				if (!cpuInstruction.setArgumentValue(i, currentByte))
				{
					disassemblyResult.first = EDisassemblyResult::WrongCPUInstructionArgument;
					return disassemblyResult;
				}
				++byteNumber;
			}
		}
		Logger::logMessage("\t" + getInstructionString(cpuInstruction), false);
	}
	if (!romFileReader.isEOF())
	{
		disassemblyResult.first = EDisassemblyResult::FileReadError;
	}
	
	return disassemblyResult;
}

std::string getDisassemblyErrorMessage(const std::pair<EDisassemblyResult, std::string>& disassemblyResult)
{
	switch(disassemblyResult.first)
	{
		case EDisassemblyResult::CantOpenROMFile:
			return "Can't open ROM file!";
		case EDisassemblyResult::FileReadError:
			return "File read error!";
		case EDisassemblyResult::UnexpectedEOF:
			return "Unexpected end of file!";
		case EDisassemblyResult::UnrecognizedCPUInstruction:
			return std::string("Unrecognized CPU instruction: ") + disassemblyResult.second;
		case EDisassemblyResult::WrongCPUInstructionArgument:
			return "Wrong CPU instruction argument!";
		default:
			return "Unknown error!";
	}
}
