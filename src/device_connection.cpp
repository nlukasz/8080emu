#include "device_connection.hpp"

bool InputDeviceConnection::getInputByte(uint8_t& outByte) const
{
	return externalDevice.readByteAtPort(getExternalDevicePort(), outByte);
}

bool OutputDeviceConnection::passOutputByte(const uint8_t inByte) const
{
	return externalDevice.writeByteAtPort(inByte, getExternalDevicePort());
}
