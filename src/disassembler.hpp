#ifndef disassembler_hpp
#define disassembler_hpp

#include <string>
#include <utility>
#include "instruction_data.hpp"
#include "rom_file_reader.hpp"

enum class EDisassemblyResult
{
	Success = 0,
	CantOpenROMFile,
	FileReadError,
	UnexpectedEOF,
	UnrecognizedCPUInstruction,
	WrongCPUInstructionArgument
};

std::pair<EDisassemblyResult, std::string> writeDisassembledData(std::string inROMFilePath, unsigned int inReadBufferSize, const uint16_t inProgramStartAddress = 0);

std::string getDisassemblyErrorMessage(const std::pair<EDisassemblyResult, std::string>& disassemblyResult);

#endif /* disassembler_hpp */
