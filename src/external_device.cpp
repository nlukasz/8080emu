#include "external_device.hpp"

ExternalDevice::~ExternalDevice()
{
	stopExternalDevice();
}

void ExternalDevice::initializeExternalDevice()
{
	if (!deviceInitialized)
	{
		doExternalDeviceInitialization();
		deviceInitialized = true;
	}
}

void ExternalDevice::startExternalDevice()
{
	initializeExternalDevice();
	if (externalDeviceSettings.usesExternalThreadLoop && (deviceLoopExternalThread == nullptr))
	{
		deviceLoopExternalThread = new std::thread(&ExternalDevice::startExternalThreadLoop, this);
	}
}

void ExternalDevice::stopExternalDevice()
{
	if (deviceLoopExternalThread != nullptr)
	{
		stopExternalThreadLoop();
		deviceLoopExternalThread->join();
		delete deviceLoopExternalThread;
		deviceLoopExternalThread = nullptr;
	}
}

bool ExternalDevice::readByteAtPort(const uint8_t inPort, uint8_t& outByte) const
{
	if (!isReadPortAvailable(inPort))
	{
		return false;
	}
	auto outputByteIterator = readBytes.find(inPort);
	if (outputByteIterator == readBytes.end())
	{
		return false;
	}
	outByte = outputByteIterator->second;
	return true;
}

bool ExternalDevice::writeByteAtPort(const uint8_t inByte, const uint8_t inPort)
{
	if (!isWritePortAvailable(inPort))
	{
		return false;
	}
	writeBytes[inPort] = inByte;
	handleByteWriteAtPort(inPort);
	return true;
}

bool ExternalDevice::isReadPortAvailable(const uint8_t inPort) const
{
	if (externalDeviceSettings.availableReadPorts.size() == 0)
	{
		return true;
	}
	return (externalDeviceSettings.availableReadPorts.find(inPort) != externalDeviceSettings.availableReadPorts.end());
}

bool ExternalDevice::isWritePortAvailable(const uint8_t inPort) const
{
	if (externalDeviceSettings.availableWritePorts.size() == 0)
	{
		return true;
	}
	return (externalDeviceSettings.availableWritePorts.find(inPort) != externalDeviceSettings.availableWritePorts.end());
}

bool ExternalDevice::canGenerateInterrupts() const
{
	return externalDeviceSettings.generatesInterrupts;
}

bool ExternalDevice::consumeInterrupt(uint8_t& outInstructionByte)
{
	if (externalDeviceSettings.generatesInterrupts && generateInterrupt)
	{
		generateInterrupt = false;
		outInstructionByte = interruptInstructionByte;
		return true;
	}
	return false;
}

uint32_t ExternalDevice::performMainThreadUpdate()
{
	if (!externalDeviceSettings.usesMainThreadLoop)
	{
		return 0;
	}
	
	std::chrono::time_point<std::chrono::steady_clock> timeNow = std::chrono::steady_clock::now();
	float timeSinceLastUpdate = 0.0f;
	if (firstMainThreadUpdateFinished)
	{
		std::chrono::duration<float> durationSinceLastUpdate = timeNow - lastMainThreadUpdateTime;
		timeSinceLastUpdate = durationSinceLastUpdate.count();
	}
	else
	{
		firstMainThreadUpdateFinished = true;
	}
	lastMainThreadUpdateTime = timeNow;
	
	return mainThreadDeviceUpdate(timeSinceLastUpdate);
}

const ExternalDeviceSettings ExternalDevice::getExternalDeviceSettings() const
{
	return externalDeviceSettings;
}

bool ExternalDevice::getWriteByteAtPort(const uint8_t inPort, uint8_t& outByte) const
{
	auto inputByteIterator = writeBytes.find(inPort);
	if (inputByteIterator == writeBytes.end())
	{
		return false;
	}
	outByte = inputByteIterator->second;
	return true;
}

void ExternalDevice::startExternalThreadLoop()
{
	externalThreadLoopRunning = true;
	lastExternalThreadUpdateTime = std::chrono::steady_clock::now();
	std::chrono::time_point<std::chrono::steady_clock> timeNow;
	std::chrono::duration<float> timeSinceLastUpdate;
	
	while (externalThreadLoopRunning)
	{
		timeNow = std::chrono::steady_clock::now();
		timeSinceLastUpdate = timeNow - lastExternalThreadUpdateTime;
		lastExternalThreadUpdateTime = timeNow;
		if (externalThreadDeviceUpdate(timeSinceLastUpdate.count()) != 0)
		{
			externalThreadLoopRunning = false;
			return;
		}
	}
}

void ExternalDevice::stopExternalThreadLoop()
{
	externalThreadLoopRunning = false;
}
