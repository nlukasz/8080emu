#include "dynamic_library_handler.hpp"

DynamicLibraryHandler::DynamicLibraryHandler(const std::string inDynamicLibraryPath)
{
	dynamicLibraryHandle = dlopen(inDynamicLibraryPath.c_str(), RTLD_LAZY);
}

DynamicLibraryHandler::~DynamicLibraryHandler()
{
	if (dynamicLibraryHandle != nullptr)
	{
#if defined(__unix__) || defined(__APPLE__)
		dlclose(dynamicLibraryHandle);
#else
		// TODO: implement clean-up on Windows
#endif
		dynamicLibraryHandle = nullptr;
	}
}

bool DynamicLibraryHandler::isDynamicLibraryValid() const
{
	return (dynamicLibraryHandle != nullptr);
}
