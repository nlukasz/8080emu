#ifndef external_devices_manager_hpp
#define external_devices_manager_hpp

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include "external_device.hpp"
#include "processor.hpp"

class ExternalDeviceManager
{
public:
	~ExternalDeviceManager();
	
	bool setupConnectionsInProcessorAndStartDevices(Processor& inProcessor);
	void stopExternalDevices();
	bool consumeExternalDevicesInterrupts(uint8_t& outIntructionByte);
	std::pair<uint32_t, const ExternalDevice*> updateExternalDevicesMainThreadLoop();
	
private:
	void removeExternalDevice(const unsigned int inIndex);
	void loadExternalDevicesConfiguration(std::vector<std::string>& outDevicesLibraries,
										  std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& outReadConnectionsMapping,
										  std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& outWriteConnectionsMapping,
										  std::unordered_set<std::string>& outDevicesGeneratingInterrupts
										  );
	bool validateMappings(const std::string inDeviceLibrary,
						  const std::unordered_map<std::string,
						  std::vector<std::pair<uint8_t, uint8_t>>>& inConnectionsMapping,
						  std::unordered_set<uint8_t>& inUsedProcessorPorts, std::unordered_set<uint8_t>& inUsedDevicePorts
						  ) const;
	bool validateConnectionsMappings(const std::vector<std::string>& inDevicesLibraries,
									 const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inReadConnectionsMapping,
									 const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inWriteConnectionsMapping,
									 const std::unordered_set<std::string>& inDevicesGeneratingInterrupts
									 ) const;
	bool constructExternaDevice(const std::string inDeviceLibrary,
								const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inReadConnectionsMapping,
								const std::unordered_map<std::string, std::vector<std::pair<uint8_t, uint8_t>>>& inWriteConnectionsMapping,
								const std::unordered_set<std::string>& inDevicesGeneratingInterrupts,
								const Processor& inProcessor
								);
	bool initializeExternalDevices(const Processor& inProcessor);
	
	std::vector<ExternalDevice*> externalDevices;
	// Pair: first = processor port, second = external device port
	std::unordered_map<const ExternalDevice*, std::vector<std::pair<uint8_t, uint8_t>>> readConnectionsMapping;
	std::unordered_map<const ExternalDevice*, std::vector<std::pair<uint8_t, uint8_t>>> writeConnectionsMapping;
	std::unordered_set<ExternalDevice*> devicesGeneratingInterrupts;
	std::unordered_set<ExternalDevice*> devicesHavingMainThreadLoop;
};

#endif /* external_devices_manager_hpp */
