#ifndef processor_h
#define processor_h

#include <cinttypes>
#include "device_connection.hpp"
#include "memory.hpp"

struct ProcessorFlags
{
	bool S = false;		// Sign
	bool Z = false;		// Zero
	bool AC = false;	// Auxiliary Carry
	bool P = false;		// Parity
	bool C = false;		// Carry
};

class Processor
{
public:
	static const unsigned int MEMORY_SIZE = 65536;
	static const unsigned int CPU_FREQUENCY = 2000000;
	
	// Constructors
	Processor(uint16_t inInitialProgramCounter = 0) : PC(inInitialProgramCounter) {}
	
	// Getters
	// Registers getters
	uint16_t getProgramCounter() const;
	uint16_t getBCPairValue() const;
	uint16_t getDEPairValue() const;
	uint16_t getHLPairValue() const;
	
	// Memory getters
	const Memory& getMemory() const;
	bool getByteAtProgramCounter(uint8_t& outByte) const;
	bool getByteAtAddressStoredInHL(uint8_t& outByteValue) const;
	bool getBytesPairStoredAtSP(uint8_t& outHighByte, uint8_t& outLowByte) const;
	bool getByteInMemory(const uint16_t inIndex, uint8_t& outByte) const;
	bool getByteInMemory(const uint8_t inAddressHighByte, const uint8_t inAddressLowByte, uint8_t& outByte) const;
	
	// Other getters
	bool areInterruptsEnabled() const;
	bool isHalted() const;
	
	// Setters
	// Registers setters
	void incraseProgramCounterBy();
	void setHLPairValue(const uint16_t inNewPairValue);
	void setPC(const uint8_t inHighByte, const uint8_t inLowByte);
	
	// Flags update
	void updateFlagsForByte(const uint8_t inByte);
	void updateFlagsForAccumulator();
	
	// Memory setters
	bool setByteInMemory(const uint16_t inMemoryIndex, const uint8_t inByte);
	bool setByteInMemory(const uint8_t inAddressHighByte, const uint8_t inAddressLowByte, const uint8_t inByte);
	bool setByteAtAddressStoredInHL(const uint8_t inByte);
	
	// Stack operations
	bool pushBytesPairOnStack(const uint8_t inHighByte, const uint8_t inLowByte);
	bool pushOnStack(const uint16_t inValue);
	bool popFromStackToBytes(uint8_t& outHighByte, uint8_t& outLowByte);
	bool popFromStack(uint16_t& outValue);
	
	// Other setters
	void setInterruptsEnabled(const bool inEnabled);
	void setHalted(const bool inHalted);
	
	// External devices connections
	bool addInputDeviceConnection(const uint8_t inPort, InputDeviceConnection inConnection);
	bool addOutputDeviceConnection(const uint8_t inPort, OutputDeviceConnection inConnection);
	bool hasInputDeviceConnectedToPort(const uint8_t inPort) const;
	bool hasOutputDeviceConnectedToPort(const uint8_t inPort) const;
	const InputDeviceConnection& getInputDeviceConnectionAtPort(const uint8_t inPort) const;
	const OutputDeviceConnection& getOutputDeviceConnectionAtPort(const uint8_t inPort) const;
	
#ifdef DEBUG_MESSAGES_ENABLED
	// Debug functions
	void printCPUState() const;
#endif
	
	// Registers
	uint8_t A = 0;
	ProcessorFlags Flags;
	
	uint8_t B = 0;
	uint8_t C = 0;
	
	uint8_t D = 0;
	uint8_t E = 0;
	
	uint8_t H = 0;
	uint8_t L = 0;
	
	uint16_t SP = 0;	// Stack Pointer
	
private:
	uint16_t PC = 0;	// Program Counter
	Memory memory = Memory(MEMORY_SIZE);
	bool interruptsEnabled = false;
	std::unordered_map<uint8_t, InputDeviceConnection> connectedInputDevices;
	std::unordered_map<uint8_t, OutputDeviceConnection> connectedOutputDevices;
	bool halted = false;
};

#endif /* processor_h */
