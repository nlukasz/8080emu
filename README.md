# 8080 Emulator

Written in C++, Intel 8080 emulator created  to learn more about making emulators. The goal of this project is to create complete, modular and open 8080 emulation platform with plugins support.

## State of the project

- All 8080 opcodes are implemented
- Emulator is passing following tests:
    - cpudiag
    - 8080PRE
    - TST8080
    - CPUTEST
- Emulator is passing all tests in 8080EXER/8080EXM except `aluop <b,c,d,e,h,l,m,a>`
- Space Invaders is working! 👾

## How to build

- Use CMake to generate project for your IDE of choice
- Currently project depends on SDL2 and SDL2_mixer

## Future plans

- Make emulator passing all tests in 8080EXER/8080EXM
- Build plugins system for external devices, and move all Space Invaders specific devices code into separate plugins
- Change emulator to a library
- Replace SDL with something better

## License

MIT
